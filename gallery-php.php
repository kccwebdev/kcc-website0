<?php 
require("includes/helpers.php");
/**
* Gallery class
*/
/*class Gallery
{
	
	function __construct(argument)
	{
		# code...
	}

	
}*/
	$type = (isset($_POST["type"])) ? $_POST["type"] : "gallery";

	/********************************Facebook********************************************/
	define('FACEBOOK_SDK_V4_SRC_DIR', '/libs/facebook-php-sdk/src/Facebook/');
	
	require('libs/facebook-php-sdk/autoload.php');
	use Facebook\FacebookSession;
	use Facebook\FacebookRequest;
if($type == "gallery"){
		$gallery;
		#get albums
		$albums = query("SELECT * FROM `albums` ");
		foreach ($albums as $album_key => $album) {
			if ($album['type']=='facebook') {
				# facebook
				//extract the description to details
				try {			
					
					$details;
					$det_commas = explode(',',$album['description']);
					foreach ($det_commas as $det_key => $detail) {
						# code...
						$det_colon = explode(':',$detail);
						$details[$det_colon[0]] = $det_colon[1];
						//pre($details);
					}

					FacebookSession::setDefaultApplication('1533598003587268','7b08640f4146447c470de9a9a7d593cf');

					// If you already have a valid access token:
					$session = new FacebookSession('1533598003587268|vAIK4Zjk5Yk5omwEsWBTnR26E4s');
					$request = new FacebookRequest(
					  $session,
					  'GET',
					  '/'.$details['album_id'].'/photos'
					 );
					$response = $request->execute();
					$graphObject = $response->getGraphObject();
					//pre($graphObject);
					$cimages = $graphObject->getProperty('data')->getPropertyNames();
					$fbimages = $graphObject->getProperty('data');
					$images;
					  $path = $fbimages->getProperty(rand(0,count($cimages)))->getProperty('images')->getProperty(8)->getProperty('source');//pic 8 ->225x400
					  //pre($path);
					  $thumb =  $path;
					$album['description'] = $details['description'];
				} catch (Exception $e) {
					
				}
			}else{
				//add videos to albums
				$images = query("SELECT * FROM `media_items` as i
									INNER JOIN `media` AS m
										ON i.item = m.id
								 WHERE `album` = ? ",$album['id']);
				$thumb = $images[array_rand($images,1)]['path'];
				
			}
			try {		
				$gallery[$album_key] =  array('id' => $album['id'],
											'name' => $album['name'],
											'type' => $album['type'],
											'description' => $album['description'],
											'thumb' => $thumb );
				//pre($gallery);
			} catch (Exception $e) {
				
			}
		}

		#make album thumbs list
		
	echo "<section id=\"gallery-content\">";
        require(VIEWS_PATH . "gallery-php.php");
    echo "</section> <!-- end gallery -->";
}else if($type == "album"){
	$album_id = (isset($_POST["id"])) ? $_POST["id"] : null;
	getAlbum($album_id);
}
	
	
	function getAlbum($id){
		$album = query("SELECT * FROM `albums` WHERE `id` = $id");
		$album = $album[0];
		if ($album['type']=='facebook') {
				# facebook
				//extract the description to details
				try {			
					
					$details;
					$det_commas = explode(',',$album['description']);
					foreach ($det_commas as $det_key => $detail) {
						# code...
						$det_colon = explode(':',$detail);
						$details[$det_colon[0]] = $det_colon[1];
						//pre($details);
					}

					FacebookSession::setDefaultApplication('1533598003587268','7b08640f4146447c470de9a9a7d593cf');

					// If you already have a valid access token:
					$session = new FacebookSession('1533598003587268|vAIK4Zjk5Yk5omwEsWBTnR26E4s');
					$request = new FacebookRequest(
					  $session,
					  'GET',
					  '/'.$details['album_id'].'/photos'
					 );
					$response = $request->execute();
					$graphObject = $response->getGraphObject();
					//pre($graphObject);
					$cimages = $graphObject->getProperty('data')->getPropertyNames();
					$fbimages = $graphObject->getProperty('data');
					$images;
					foreach ($cimages as $ckey => $cimage) {
					  # code...
					  $src = $fbimages->getProperty($ckey)->getProperty('images')->getProperty(8)->getProperty('source');//pic 8 ->225x400
					  //pre($src);
					  $images[$ckey] = array('album' => $id,
					  						'type' => 'facebook image',
					  						'path' => $src,
					  						'information' => '');
					}
				} catch (Exception $e) {
					
				}
			}else{
				//add videos to albums
				$images = query("SELECT * FROM `media_items` as i
									INNER JOIN `media` AS m
										ON i.item = m.id
								 WHERE `album` = ? ",$album['id']);
				
				
			}
				//pre($images);
			#make image thumbs grid & slideshow i.e. 3d content
			#make image thumbs grid
				echo("<div class=\"image-grid\">
		        		<ul  class=\"image-thumbs gallery-thumbs da-thumbs \">
		        			<li class=\"grid-sizer\"></li><?/* for Masonry column width */?>");
							foreach ($images as $image_key => $image) {
								# image thumb list
								echo ("<li class=\" \">
							              <a href=\"#gallery-".($id)."-".($image_key+1)."\">
							                <img src=\"".$image['path']."\" />
							                <div><span>".$image['information']."</span></div>
							              </a>
							           </li>");
							}
				echo("</ul>
		  			</div>");

			#make image slideshow
				echo("<section id=\"images-1\" class=\"slideshow\">
		          		<ul>");
					foreach ($images as $vimage_key => $image) {
								# image slideshow list
								echo ("<li>
							              <figure>
							                <figcaption>
							                  <h3>Image Name</h3>
							                  <p>Image description: ".$image['information']."</p>
							                </figcaption>
							                <img src=\"".$image['path']."\" alt=\"img\"/>
							              </figure>
							          </li>");
							}
				echo ("</ul>
				        <nav>
				            <span class=\"icon nav-prev glyphicon glyphicon-chevron-left\"></span>
				            <span class=\"icon nav-next glyphicon glyphicon-chevron-right\"></span>
				            <span class=\"icon nav-close glyphicon glyphicon-remove\"></span>
				        </nav>
			      		<div class=\"info-keys icon\">Navigate with arrow keys</div>
		     		</section>");
			
	
	}
			/*album numbering by id and make 'gallery not available' display
			*close iconand other icons
			*gallery slideshow hanging
			*
			* tab.js if gallery-# does not exist, redirect
			* ideally albums should be selected by their id
			* all ablum informatio should then be at one place
			*/
	

?>