<?

    require_once("includes/helpers.php");
    
    if(isset($_GET['action'])){
    $action=$_GET['action'];
    require("adminaction.php");
        switch($action)
	    {
		    case 'addnews':
			    addNews($_POST['title'],$_POST['info'],$_POST['sdate'],$_POST['edate']);
			    break;

            case 'services':
			    render('services', array('title' => 'KCC-'.ucfirst($page)));
			    break;

             case 'members':
			    render('members', array('title' => 'KCC-'.ucfirst($page)));
			    break;

            default:
			    render('errors/404', array('title' => 'Error 404'));
			    break;
	    }

    }
    
    function generateCarouselEditor()
    {
        $images = query("SELECT * FROM `media` WHERE `type`='carousel'");

        foreach($images as $image)
        {
            extract($image);
            echo "<li class=\"list-group-item\">";
                echo "<form class=\"form-inline\" role=\"form\">";
                    echo "<input class=\"form-control\" id=\"information\" type=\"text\" value=\"$information\">";
                    echo "<input class=\"form-control\" id=\"path\" type=\"text\" value=\"$path\">";
                    echo "<button class=\"btn btn-danger\">" . glyphicon("minus-sign") . "</button>";
                echo "</form>";
            echo "</li>";
        }
    }
    
    
    
    
    //prepare page
    $html = require_string(VIEWS_PATH . "admin.php");

    //prepare response to ajax query
    $json = array("status" => 200, "page" => "admin", "html" => $html);

    //return json
    echo json_encode($json);

    //exit for good measure
    exit();
?>
