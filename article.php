<?php

    require("includes/helpers.php");

    //gets individual articles from the db
    
        if(isset($_POST["id"]))
        {
            $id = $_POST["id"];
            //news without articles have no 'Read more' links 
            $data = query("SELECT * FROM `articles` AS a
                           WHERE a.id = $id LIMIT 1");
            $data = $data[0];
            $date = date('l jS F Y',strtotime($data['date_published']));
            //pre($data);  
            
            //get publisher
            if($data['publisher']!=null&&is_numeric($data['publisher'])){
                $publisher = query("SELECT * FROM `people` WHERE `id` = ? ",$data['publisher']);
                //Make link for profile view or view of article published?
                $publisher = $publisher[0]['firstname'].' '.$publisher[0]['lastname'];
            }else{
                $publisher = "Karen Community Church";
            }
        }
        function showNewsItem($id){
            $today = date("Y-m-d H:i:s");
            $news = query("SELECT * FROM `news_items`
                           WHERE `article` = $id LIMIT 1");
            $news = (isset($news[0]))? $news[0] : null;
            if (isset($news['information'])&&($today<$news['expirydate']) ){
                echo 
                "<div id=\"article-news\"> 
                    <h2>News:</h2>
                    <t><p id=\"news-info\">".$news['information']."</p>
                    <hr>
                </div>";
            }
            
        }
        $sampletxt= "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    //render the actual view

    echo "<section id=\"article-content\">";
        require(VIEWS_PATH . "article.php");
    echo "</section> <!-- end article -->";

?>
