<?

    require_once("includes/helpers.php");

    function errorJSON()
    {
        //prepare json response
        $json = array("status" => 500);

        //return failure to client
        echo json_encode($json);

        //exit file
        exit();
    }

    //calls login
    function accessLogin()
    {
        if(isset($_SESSION["id"]))
        {
            accessAdmin();
        }
        else
        {
            accessLogout();
        }
    }

    //calls logout
    function accessLogout()
    {
        require("logout.php");
        accessJSON(); //failure
    }

    //calls admin-access
    function accessAdmin()
    {
        require("admin.php");
        accessJSON(); //failure
    }
    
    //first split is based on session
    if(isset($_SESSION["id"]))
    {
        file_put_contents('php://stderr', print_r('Session Exists', TRUE));
        switch($_POST["page"])
        {
            case "login":
                accessLogin(); 
                break;
            case "logout":
                accessLogout();
                break;
            case "admin":
                accessLogin(); 
                break;
        }

    }
    else
    {
        require("login.php");
        errorJSON(); //failure
    }



?>
