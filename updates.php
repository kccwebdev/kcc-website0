<?php

    require_once("includes/helpers.php");

    $date = date("Y-m-d H:i:s");

    $news = query("SELECT * FROM `news_items` WHERE `expirydate` > ? ORDER BY `startdate` DESC LIMIT 5", $date);
    $articles = query("SELECT * FROM `articles` ORDER BY `date_published` DESC LIMIT 10"); 

    
    function trimLength(&$string, $length)
    {
        //leave string as is if length <= length
        if(strlen($string) <= $length) return;

        //shorten string to length
        $string = substr(ltrim($string), 0, $length);

        //trim string if necessary, storing whether or not in whole
        if($whole = (substr($string, -1) == " ")) $string = trim($string);

        //split up string
        $split = explode(" ", $string);

        //assemble string
        $string = implode(" ", $whole ? $split : array_splice($split, 0, sizeof($split) - 1)) . "...";

    }

    function getDateFromRaw($item, $format = 'j/n/y')
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $item);
        return $date->format($format);
    }

    function getTimeFromRaw($item)
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $item);
        return $date->format('H:i');
    }
?>
