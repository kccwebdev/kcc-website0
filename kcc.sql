-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2015 at 07:02 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kcc`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `type` varchar(15) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name`, `type`, `description`) VALUES
(1, 'DVBS April 2015', 'facebook', 'album_id:777046382411343,description:Daily Vacation Bible Study'),
(2, 'Church Photos', 'local', 'Miscellaneous church photos');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `category` tinytext NOT NULL,
  `date_published` datetime NOT NULL,
  `publisher` int(11) DEFAULT NULL,
  `data` longtext NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `name`, `category`, `date_published`, `publisher`, `data`, `views`) VALUES
(1, 'Launch of the KCC website', '', '2015-05-20 21:05:11', NULL, 'We finally have the new KCC website up and running. View news updates, listen to sermons and view our huge gallery of photos on the website.', 0),
(2, 'Launch of the Aweer Bible', '', '2015-05-20 21:05:11', NULL, 'The Aweer after several years finally have the gospel of Luke in their language, ''Injili ya Luka''', 0),
(3, 'Prayers  and Fasting - Week 1 (June 18th - June 28)', '', '2015-06-15 00:00:00', NULL, 'On launching the June teaching series, PRAYING WITH CONFIDENCE, we are also launching a 40 day prayer and fasting period starting 18th June. Join other congregants as we fast and pray.\nPrayer for transformation at individual and congregation level.Below is are prayer items for the first week of prayer and fasting.\n<table border="1" cellspacing="1">\n		<thead>\n        	<tr>\n                <th>Date</th>\n                <th>Prayer Item</th>\n                <th>Scripture</th>\n	    </thead>\n    	<tbody>\n			<tr >\n				<td>Thursday June 18</td>\n				<td>Pray for KCC elders and core team leaders on a regular basis: for grace, faithfulness. good health, their families, integrity, humility</td>\n				<td>1 Timothy 2: 2-4<br>Jeremiah 3:15<br>Colossians 3:23</td>\n			</tr>\n			<tr >\n				<td>Friday June 19</td>\n				<td>Pray for all believers at KCC to stand as a witness for Christ</td>\n				<td>John 17:20-23<br>Ephesians 6:18</td>\n			</tr>\n			<tr >\n				<td>Saturday June 20</td>\n				<td>Pray for the pastoral team as they serve the people of God at KCC</td>\n				<td>1 Thessalonians 5:17-18<br>Collosians 4:2<br>2 Timothy 4:2</td>\n			</tr>\n			<tr >\n				<td>Sunday June 21</td>\n				<td>Pray for KCC gospel partners for continues fellowship and unity of purpose:<br>1. Peninsula Community Church</td>\n				<td>Hebrews 13:17<br>1 Corinthians 15:58</td>\n			</tr>\n			<tr >\n				<td>Monday June 22</td>\n				<td>Pray for KCC Partners:<br>2. South Shore Baptist Church USA</td>\n				<td>2 Peter 3:18<br>3 John 1:2</td>\n			</tr>\n			<tr >\n				<td>Tuesday June 23</td>\n				<td>Pray for KCC Partners:<br>3. Bible Translation and Literacy(BTL) and others</td>\n				<td>2 Timothy 4:5</td>\n			</tr>\n			<tr >\n				<td>Wednesday June 24</td>\n				<td>Pray for God''s provision for projects and thanksgiving for fundraising done so far for the tabernacle project.</td>\n				<td>Philippians 4:6<br>Psalms 106<br>Psalms 107</td>\n			</tr>\n			<tr >\n				<td>Thursday June 25</td>\n				<td>Pray for the 10 million in 10 months we are trusting God for towards the completion of the children ministry centre.</td>\n				<td>Philippians 4:19<br>Mark 9:23</td>\n			</tr>\n			<tr >\n				<td>Friday June 26</td>\n				<td>Boresha Bul: We are trusting the Lord for the provision of Kshs 600,000 towards the 2015 Boresha Bul.</td>\n				<td>2 Timothy 4:2<br>James 1:27</td>\n			</tr>\n			<tr >\n				<td>Saturday June 27</td>\n				<td>We are trusting the Lord to provide 1.25 million every month to cater for ministry needs.</td>\n				<td>2 Corinthians 9:8<br>Luke 1:37<br>Ephesians 3:20-21</td>\n			</tr>\n			<tr >\n				<td>Sunday June 28</td>\n				<td>Prayer for the sick and hurting in our midst: Dymphna Kitololo who is on treatment in India, Harriet Kinyua, Jane Kiarie, Rebecca Mutungi, Mrs Nguru, Terry''s mother and others </td>\n				<td>Exodus 15:22-27<br>James 5:15-16<br>Matthew 4:23</td>\n			</tr>\n		</tbody> \n	</table>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `article_categories`
--

CREATE TABLE IF NOT EXISTS `article_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `description`) VALUES
(1, 'Image Gallery', 'KCC Site gallery'),
(2, 'tiral2', 'trial2 gallery'),
(3, 'trial3', 'trial3 gallery'),
(4, 'trial4', 'trial4 gallery'),
(5, 'about', 'images for the about page');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL COMMENT 'from categories',
  `leader` int(11) DEFAULT NULL COMMENT 'from people',
  `bio` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`,`leader`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `category`, `leader`, `bio`) VALUES
(1, 'Main Service', 'ministries', 1, 'The "adult" service in the main sanctuary.'),
(2, 'Ground Zero', 'ministries', 2, ''),
(3, 'Tabernacle', 'ministires', NULL, 'Sunday School -- children''s ministry.'),
(4, 'pastors', '', 1, NULL),
(5, 'elders', '', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL COMMENT 'from categories',
  `information` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `category`, `information`) VALUES
(1, 'welcome_message', 'Church Information', 'KCC is a church located in the Karen-Ngong-Langata area with a heartfelt desire to see the Great Commission fulfilled through actively engaging the community surrounding it.'),
(2, 'example', 'Developer Text', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. '),
(3, 'contact_message', 'Church Information', 'To contact KCC, you can access any of the below information, or fill out the contact box.'),
(4, 'church_email', 'Contact Information', 'kcc@karencommunitychurch.org'),
(5, 'church_numbers', 'Contact Information', '+254-732923432\r\n+254-734533434'),
(6, 'complete_contact_message', 'Contact Information', 'When you''re done, hit the button above and KCC''s administrators will attempt to help you in whatever ways they can.'),
(7, 'login_message', 'Login Information', 'To login, enter your information below.'),
(8, 'complete_login_message', 'Login Information', 'When you''re done, click the button above to log in.'),
(9, 'headline', '', 'Welcome to Karen Community Church'),
(10, 'introduction', '', 'Are you looking for a family oriented community church? Do you have a heart and passion for missions? For both young and old KCC will give you a chance to grow and serve.'),
(11, 'mission', '', 'To grow a caring community of Christ''s disciples \nwho impact our world through missions and evangelism'),
(12, 'vision', '', 'Believers who know, love, and present Christ as Lord'),
(13, 'statement_of_faith', '', 'Our statement of faith communicates what we believe about God, the church, and mankind. The essential truths of the Christian faith from the Bible that we expect all members to agree on '),
(14, 'history', '', 'Karen Community Church (KCC) started in September 1996 by a group of around 18 people, commissioned by the Nairobi Baptist Church to plant a new church in Karen. It started with worship and Sunday school services at the Kenya Commercial Bank (KCB) Management Institute along Ngong View, off Ngong Road. The congregation has since grown to over 300 people, including children, youth, and adults. The worship services for these three groups are commonly called Children''s Tabernacle, Ground Zero (Youth) and Family Church (Adults). We follow a congregation-led church model where the members constitute the top governing body. They do so through the various Members'' Days such as the Annual General Meeting or through their elected representatives, the Elder''s Court.\n\nMissions, evangelism, discipleship and fellowship have remained the core engines of the church, and are indeed what makes KCC unique. The sense of community is embodied in the church motto ''a place to belong, a place to become'', and has leveraged members to active participation in the life of the church.\n\nThe church is fondly referred to as mission mobilized to emphasize its strong commitment to missions. Our first missionary outreach was to the Aweer (Boni) people of the Kenyan coast who are predominantly unreached by the gospel. Our cross cultural missions have taken us to as far as China, and nearer to the indigenous Kenyan communities of the arid northern Kenya region (Oltrurot). Our local mission work has seen us actively minister and evangelize in our low income neighborhood of Bulbul.\n\nLife Groups are the heartbeat of the church. These home based fellowships meet regularly to study God''s word, fellowship and support each other. The church provides spiritual and technical support to the Life Groups including capacity building for the leaders and Bible study material.\n\nMuch more can be said about KCC, including major milestones and high and low moments as would be expected of any organization, Christian or otherwise. However, it must be said that the Lord has remained truly unfailing in His grace and steadfast in His love. Philippians 1:6, our key verse for the prior strategic season, provided us with strength, reassurance and hope.  The Lord has been bringing to completion the good work he began with us as we made the step of faith to plant a church in Karen. We embrace our new key verse that implores us to encourage each other and build each other up (1Thessolanians 5:11) as we wait upon the Lord to help us accomplish our mission and strategic objectives for the next five years, to His glory and honor. Amen\n'),
(15, 'adults', 'ministries', 'Adults are the more mature part of our congregation. They are men and women over 24 years old in diverse vocations. We provide teaching, relationship, and service opportunities for them to grow in and to live out their walk with and calling from God.<br>Components:-Worship: Preaching, Music, Prayer, Ushering, Set Up, and Ordinances\nFellowship and Discipleship: Family Groups\nDiscipleship: School of Marriage & Family'),
(16, 'youth', 'ministries', 'Youth are a vibrant and energetic part of our congregation with unique needs and opportunities. They are young women and men aged 13 to 24. We develop teaching, relationships, and experiences to help them mature in their spiritual and personal lives.\n<br>\nComponents:- Worship: Preaching, Music, Prayer, Ushering, Set Up, and Ordinances\nFellowship and Discipleship: Youth Ministries\nDiscipleship: School of Life'),
(17, 'children', 'ministries', 'Children are a vital and inquisitive part of our congregation with unique needs & opportunities. Children are boys and girls up to 13 years. We prepare and deliver a curriculum and experiences appropriate to their ages that helps them to know, love, and present Jesus as Lord. We also train and equip parents to promote the personal and spiritual development of their children.<br>Components:-Worship: Sunday School, Music, Prayer, Greeting, Set Up, and Ordinances\nFellowship and Discipleship: Children''s Groups\nDiscipleship: School of Parenting');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL COMMENT 'from categories',
  `path` varchar(500) NOT NULL,
  `information` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `type`, `path`, `information`) VALUES
(1, 'youtube', 'b6iCQX053AM', 'Sermon on 22nd March by Ambola'),
(2, 'video', 'videos/Community.319.mp4', ''),
(3, 'video', 'videos/Community.319.mp4', ''),
(4, 'video', 'videos/Community.319.mp4', ''),
(5, 'video', 'videos/Community.319.mp4', ''),
(6, 'video', 'videos/Community.319.mp4', ''),
(7, 'carousel', 'imgs/carousel/compound.jpg', 'Church Compound'),
(8, 'carousel', 'imgs/carousel/congregation.jpg', 'Congregation'),
(9, 'carousel', 'imgs/carousel/news.jpg', 'News'),
(10, 'carousel', 'imgs/carousel/series.jpg', 'Series'),
(12, 'people', 'imgs/people/person.jpg', ''),
(13, 'gallery', 'imgs/gallery/ropes.jpg', 'Trial gallery image'),
(14, 'gallery', 'imgs/gallery/2.jpg', ''),
(15, 'gallery', 'imgs/gallery/preaching.jpg', ''),
(16, 'gallery', 'imgs/gallery/4.jpg', ''),
(17, 'gallery', 'imgs/gallery/5.jpg', ''),
(18, 'min_children', 'imgs/about/min-children.jpg', 'about image for the children ministry'),
(19, 'min_adults', 'imgs/about/min-adults.jpg', ''),
(20, 'min_youth', 'imgs/about/min-youth.jpg', ''),
(21, 'image', 'imgs/series/faith_forward.jpg', ''),
(22, 'youtube', 'OqlNW9KBlug', 'Sermon on 24th May 2015 by John Nganga'),
(23, 'youtube', 'ymYBX7AheMg', 'Sermon on 17th May 2015 by Mark Shaw'),
(24, 'youtube', 'dzeDTOuTAU8', 'Sermon on 3rd May 2015 by Prof. Thairu'),
(25, 'youtube', 'fEHT5yPq-pI', 'Sermon on 31st May by John Nganga'),
(26, 'youtube', 'oRGCPVRQhiU', 'Sermon on 14th June 2015 by Pastor Muita'),
(27, 'youtube', 'Fo3UEMAGESw', 'Ground Zero sermon on 14th June 2015 by Pastor Rose');

-- --------------------------------------------------------

--
-- Table structure for table `media_items`
--

CREATE TABLE IF NOT EXISTS `media_items` (
  `album` int(11) NOT NULL,
  `item` int(11) NOT NULL COMMENT 'from media',
  `gallery` int(11) NOT NULL,
  UNIQUE KEY `gallery` (`album`,`item`),
  KEY `item` (`item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_items`
--

INSERT INTO `media_items` (`album`, `item`, `gallery`) VALUES
(2, 18, 5),
(2, 19, 5),
(2, 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_items`
--

CREATE TABLE IF NOT EXISTS `news_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category` varchar(11) DEFAULT NULL COMMENT 'from categories',
  `information` text NOT NULL,
  `article` int(11) DEFAULT NULL,
  `gallery` int(11) DEFAULT NULL,
  `startdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expirydate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `news_items`
--

INSERT INTO `news_items` (`id`, `name`, `category`, `information`, `article`, `gallery`, `startdate`, `expirydate`) VALUES
(1, 'Fundraising (M.P.H.)', '2', 'The final fundraising event for the Multi-Purpose Hall will be taking place this Thursday.', NULL, NULL, '2015-05-20 17:58:18', '2014-08-31 12:34:36'),
(2, 'Camp 360', '3', 'It''s here again! Camp 360 is back for a third and final time, and this time, nothing will be held ba', NULL, NULL, '2015-05-20 17:58:24', '2014-08-25 21:00:00'),
(3, 'Kesha @ GZ', '3', 'Ever feel like the worship session on Sundays is too short? Come and join us in an overnight session', 0, NULL, '2014-10-03 13:37:41', '2014-10-30 21:00:00'),
(4, 'Joint Service', '2', 'The upcoming Sunday (20th) will be a joint service taking place in the Main Sanctuary.', 0, NULL, '2014-07-31 06:45:14', '2014-07-30 21:00:00'),
(5, 'Kids Service Leading', '4', 'In the upcoming joint service (20th) the Tabernacle will continue in their worship leadership.', 0, NULL, '2014-07-25 06:45:14', '2014-07-30 21:00:00'),
(6, 'Multi-Purpose Hall Opening', '2', 'Lorem ipsum dolor sit amet.', 0, NULL, '2014-07-30 21:00:00', '0000-00-00 00:00:00'),
(7, 'Induction Ceremony', '4', 'KCC''s newest members will be inducted on the upcoming Sunday.', 0, NULL, '2014-10-03 13:39:37', '2014-12-07 21:00:00'),
(8, 'News #4', '3', 'Content stuff about other stuff ', 0, NULL, '2014-08-16 21:00:00', '0000-00-00 00:00:00'),
(9, 'Final News Story', '2', 'And then there were sixteen of us, surrounded by logic.', 0, NULL, '2014-07-12 21:00:00', '0000-00-00 00:00:00'),
(10, 'Last Event', '4', 'This upcoming Sunday, it''s going down!', 0, NULL, '2014-10-03 13:40:37', '2014-12-24 21:00:00'),
(11, 'Church Kesha', '0', 'Your all invited to the chuch kesha', 0, NULL, '2014-08-02 11:16:47', '2014-08-07 21:00:00'),
(12, 'Teens week', '0', 'The teen''s week will be running from the 12th to 16th of August. All teens are ecouraged to attend', 0, NULL, '2014-08-02 11:21:51', '2014-08-15 21:00:00'),
(13, 'Website Launch', '0', 'The new improved kcc website is to be launched.', 1, NULL, '2015-04-01 17:26:09', '2015-05-31 21:00:00'),
(14, 'Trip to PCC', '0', 'A group of KCC women have gone to visit Peninsula Community Chapel', 0, NULL, '2014-10-03 13:05:50', '2014-10-29 21:00:00'),
(15, 'Camp 360', '0', 'The annual teens camp is coming.It will run from 7th to 13th December all teens are encouraged to re', 0, NULL, '2014-10-03 13:07:40', '2014-12-18 21:00:00'),
(16, 'Launch of the Aweer BIble', '1', 'The Aweer can now read the book of Luke in there own language', 2, 0, '2015-06-16 16:44:00', '2015-05-26 21:00:00'),
(17, 'Prayers', NULL, 'Corporate prayer and worship is going on in church every Thursday from 5.30 pm to 6.30 pm and at lunch hour from 1.00pm - 2.00pm. Welcome to church.', 0, NULL, '2015-06-11 06:58:05', '2015-06-30 21:00:00'),
(18, 'Extraordinary members meeting', NULL, 'Notice is hereby given that the KCC Extra Ordinary meeting will take place on 27th June 2015. The quorum is 60% of members , so members are kindly requested to attend', NULL, NULL, '2015-06-11 06:56:35', '2015-06-27 12:00:00'),
(19, 'Choir auditions', NULL, 'KCC choir is here! Voice Placement Auditions will take place on June 20th from 2.00pm. \nSign up at the desk if you are interested in joining this auspicious choir! See Phoebe Opiyo for details.\n', NULL, NULL, '2015-06-16 16:37:57', '2015-06-19 21:00:00'),
(20, 'Friday Cafe', NULL, 'Are you longing for a deeper time of worship and prayer? Look out for the Friday Worship Cafe! which will be launched soon.', NULL, NULL, '2015-06-16 16:37:20', '2015-06-26 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE IF NOT EXISTS `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `member` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `bio` text,
  `photo` int(11) DEFAULT NULL COMMENT 'photo id number of person',
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `firstname`, `middlename`, `lastname`, `title`, `member`, `phone`, `email`, `bio`, `photo`) VALUES
(1, 'Njomo', NULL, 'Muita', 'Senior Pastor', 1, '0721KCCKCC', 'njomo@karencommunitychurch.org', 'Muita graduated from University of Nairobi in 1997 with a BA in Anthropology. While at university, he met the Navigators who nurtured his faith and exposed him to disciple-making vision. Upon graduation, he joined their staff, serving as disciple maker leader of a number of universities situated in the greater Nairobi Area from 1997 - 2003. In 2001 Muita married his youth sweetheart Bennah Muita at Karen Community Church. In the same year, he enrolled for M. Divinity program at Nairobi International School of Theology (NIST). In September 2003, Muita joined KCC staff as a youth pastoral intern. In the following years, he served as the youth director until 2008 when he has called to office of the associate pastor. In March 2014, he assumed the role of the senior pastor.\n\nMuita is blessed with two children, Muita Njomo (11 years) and Wanjiru Njomo (8 Years). They are in the process of adopting their second son, Joseph Mwaura (20 months).\n\nMuita is committed to the study and application of God''s Word to ministry and personal life. He is determined to trust God for a God-Sized Mission. Currently, he is a part-time student at Africa International University, pursuing a MTH World Christianity.', 12),
(2, 'Godfrey', NULL, 'Mwaniki', 'Pastor', 1, '0721GODMWA', 'mwanikigodfrey@karencommunitychurch.org', '', 12),
(3, 'Mike', NULL, 'Diki', 'Pastor', 1, '0734126843', 'pstmikediki@yahoo.com', '', 12),
(4, 'Mark', NULL, 'Karanja', 'Technician', 1, '0721630033', 'markwahome@live.com', NULL, NULL),
(5, 'Mr.', NULL, 'Ambola', NULL, 0, NULL, NULL, NULL, NULL),
(6, 'Mr', NULL, 'Mwangi', NULL, 0, NULL, NULL, NULL, NULL),
(7, 'Mark', NULL, 'Shaw', 'Dr.', 0, NULL, NULL, NULL, NULL),
(8, 'John', NULL, 'Nganga', 'Mr.', 0, NULL, NULL, NULL, NULL),
(9, 'Prof.', NULL, 'Thairu', NULL, 0, NULL, NULL, NULL, NULL),
(10, 'Rose', NULL, 'Chalo', 'Pastor', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `people_lists`
--

CREATE TABLE IF NOT EXISTS `people_lists` (
  `group` int(11) NOT NULL,
  `member` int(11) NOT NULL COMMENT 'from people',
  UNIQUE KEY `unique group member` (`group`,`member`),
  KEY `member` (`member`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `people_lists`
--

INSERT INTO `people_lists` (`group`, `member`) VALUES
(4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE IF NOT EXISTS `queries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(50) NOT NULL,
  `text` text NOT NULL,
  `person` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subject` (`subject`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `series`
--

CREATE TABLE IF NOT EXISTS `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `scripture` varchar(50) NOT NULL,
  `picture` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `picture` (`picture`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `series`
--

INSERT INTO `series` (`id`, `name`, `scripture`, `picture`) VALUES
(1, 'Experiencing God', 'Psalms', 8),
(2, 'Through The Mists', 'Luke 5', 10),
(4, 'Faith Forward', 'Hebrews 11:1-2', 21),
(5, 'Praying with confidence', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sermons`
--

CREATE TABLE IF NOT EXISTS `sermons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `scripture` varchar(50) NOT NULL,
  `series` int(11) DEFAULT NULL COMMENT 'from series',
  `speaker` int(11) NOT NULL COMMENT 'from people (we add all speakers to database)',
  `audio` int(11) DEFAULT NULL COMMENT 'from gallery',
  `video` int(11) DEFAULT NULL COMMENT 'from gallery',
  `slides` int(11) DEFAULT NULL COMMENT 'from gallery',
  PRIMARY KEY (`id`),
  KEY `speaker` (`speaker`),
  KEY `series` (`series`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `sermons`
--

INSERT INTO `sermons` (`id`, `title`, `scripture`, `series`, `speaker`, `audio`, `video`, `slides`) VALUES
(1, 'Sermon Title', 'Mark 11:2-3, Psalm 119:46', 4, 5, NULL, 1, NULL),
(7, 'What is Success?', '', 4, 7, NULL, 23, NULL),
(8, 'Defining my Destiny', '', 4, 8, NULL, 22, NULL),
(9, 'Obeying His word', 'Matthew 4', 4, 9, NULL, 24, NULL),
(10, 'Obeying His word 2', '', 4, 8, NULL, 25, NULL),
(11, 'Call to prayer', '1 John 5: 14-15', 4, 1, NULL, 26, NULL),
(12, 'Call to prayer', '', 4, 10, NULL, 27, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sermon_announcements`
--

CREATE TABLE IF NOT EXISTS `sermon_announcements` (
  `sermon` int(11) NOT NULL COMMENT 'from sermons',
  `announcement` int(11) NOT NULL COMMENT 'from news',
  PRIMARY KEY (`sermon`),
  KEY `announcement` (`announcement`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ministry` int(11) NOT NULL COMMENT 'from groups',
  `music` int(11) DEFAULT NULL COMMENT 'from media',
  `sermon` int(11) NOT NULL COMMENT 'from sermons',
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sermon` (`sermon`),
  KEY `ministry` (`ministry`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `ministry`, `music`, `sermon`, `date`) VALUES
(1, 1, NULL, 1, '2015-03-22'),
(7, 1, NULL, 8, '2015-05-24'),
(8, 1, NULL, 7, '2015-05-17'),
(9, 1, NULL, 9, '2015-05-03'),
(10, 1, NULL, 10, '2015-05-31'),
(11, 1, NULL, 11, '2015-06-14'),
(12, 2, NULL, 12, '2015-06-14');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL COMMENT 'from people',
  `position` int(11) NOT NULL COMMENT 'from categories',
  PRIMARY KEY (`id`),
  KEY `position` (`position`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL COMMENT 'from people',
  `salt` char(100) NOT NULL,
  `hash` char(100) NOT NULL,
  `pass` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `salt` (`salt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `salt`, `hash`, `pass`) VALUES
(4, '', '', '11032');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `media_items`
--
ALTER TABLE `media_items`
  ADD CONSTRAINT `media_items_ibfk_1` FOREIGN KEY (`album`) REFERENCES `albums` (`id`),
  ADD CONSTRAINT `media_items_ibfk_2` FOREIGN KEY (`item`) REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `people_lists`
--
ALTER TABLE `people_lists`
  ADD CONSTRAINT `people_lists_ibfk_1` FOREIGN KEY (`group`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `people_lists_ibfk_2` FOREIGN KEY (`member`) REFERENCES `people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `series`
--
ALTER TABLE `series`
  ADD CONSTRAINT `series_ibfk_1` FOREIGN KEY (`picture`) REFERENCES `media` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
