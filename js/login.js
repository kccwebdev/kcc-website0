function loginSubmit(event) {
    var email = $('#login-form #email').val();
    var password = $('#login-form #password').val();

    /*if(email == "" || password == "") {
        loginError("You must enter an email and a password.");
        return false;
    }*/
    
    //Clear error messages
    loginError();
    
    var normal = $('#login-area').html();
    $('#login-loading').css('display', 'block');
    //$('#login-area').html($('#login-loading')).fadeIn();
    $.ajax({
                type: 'POST',
                url: 'access.php',
                data: { page: 'login', email: email, password: password },
                dataType: 'json',
                success: function(data) {
                        //all good
                        if(data.status === 200) {
                            location.reload(true);
                        }
                        else
                        {
                           //  $('#login-area').html(normal).fadeIn();
                            loginError('Login Error! Contact Administrator');
                        }
                    $('#login-loading').css('display', 'none');
                    
                }

    });

    return false;
}

function loginError(message) {
    if(message == null) {
        $('#login-form #error-message').fadeOut();
        return;
    }
    
    var outer = '<div id="error-message" class="alert alert-warning" role="alert"></div>';
    if(!$('#login-form #error-message').length) {
        $('#login-form').prepend(outer);
    }

    $('#login-form #error-message').text(message);
    $('#login-form #error-message').hide().fadeIn();

}

function logout() {
    $.ajax({
                type: 'POST',
                url: 'access.php',
                data: { page: 'logout' },
                dataType: 'json',
                success: function(data) {
                        //all good
                        if(data.status === 200) {
                            location.reload(true);
                        }
                        else
                        {
                            //error
                        }
                    
                }

    });
}








