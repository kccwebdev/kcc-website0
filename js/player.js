function videoPlayer(link_clicked) {
    var sermon = $(link_clicked).attr('sermon');
    $.ajax({
                type: 'POST',
                url: 'player.php',
                data: 'id='+sermon,
                dataType: 'html',
                success: function(data) {
                    $('#services-body').append(data);
                    $('#video-player').modal('show');
                    $('#video-player').off('hidden.bs.modal').on('hidden.bs.modal', function() {
                        $('#video-player').remove();
                    });
                }
    });

}

