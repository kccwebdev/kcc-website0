var FB;
var imgcount;
//alert('gallery js');


/****FB Connection******/
function getFB(callback){
	//console.log(FB);
	if(FB == undefined){		
		window.fbAsyncInit = function() {
	        FB.init({
	          appId      : '1533598003587268',
	          xfbml      : true,
	          version    : 'v2.3'
	        });
	      };

	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/en_US/sdk.js";
	         fjs.parentNode.insertBefore(js, fjs);

	       }(document, 'script', 'facebook-jssdk'));
          	 
	       var intvFB = setInterval(function(){
	       	if(FB != undefined){
	       		callback();
	       		clearInterval(intvFB);
	       	}else{
	       		console.log('connecting to FB...');
	       	}
	       },100);

	          
	}else{
		console.log('already defined');
		callback();
	}
}

function getGallery(){
    //get gallery view
    getFB(function(){console.log('FB defined');});/*****Remember to uncomment!!!IMPORTANT*****/
    $.get( "views/gallery-js.php", function( data ) {
            $(window).scrollTop(0)
            $('#main-content').html(data);
            $('#main-content section').hide();
            //activate link
            $('#main-header .nav_item_link').removeClass('nav_link_active');
            $('#main-header a[href="#gallery"]').addClass('nav_link_active');
                //get gallery info
                $.ajax( {
                                type: 'POST',
                                url: 'gallery-js.php',
                                data: 'type=gallery',
                                dataType: 'json',
                                success: function(albums) {
                                    //console.log(albums);
                                    $('#main-content section').hide();
                                    albums.forEach(function(album){
                                        //console.log(album);
                                        if(album.type == 'facebook'){
                                            //get fb thumb
                                            //if it takes too long to load, store a thumbnail url
                                            var details = {};
        				                            var det_commas = album.description.split(',');
        				                            det_commas.forEach(function(detail,det_key){
        				                                var det_colon = detail.split(':');
        				                                //console.log(det_colon);
        				                                details [det_colon[0]] = det_colon[1];
        				                            });
        				                            album['description'] = details.description;
				                                    //console.log(details.album_id);
                                            getFB(function(){
                                            	//console.log(FB);
                                            	var pageAccessToken = '1533598003587268|vAIK4Zjk5Yk5omwEsWBTnR26E4s';
                                            	FB.api('/'+details.album_id+'/photos', 
                										          {
                										            access_token : pageAccessToken
                										          }, 
                    										        function(response) {
                    										          	//console.log(response);
                    											        var rand = Math.floor((Math.random() * response.data.length) + 1);
                                                  //make get thumb function to make sure ascpect ratio and quality of thumb is as required
                                                  var thumb_no = response.data[rand].images.length-1;
                    											        var path = response.data[rand].images[thumb_no].source;
                    											        album['thumb'] = path;
                    											        console.log(path);
                    											        var li = "<li class='' style=\"width:48%;\">\
                                                                           <a href='#gallery-"+album.id+"'>\
                                                                               <img width='100%' src='"+album.thumb+"'/>\
                                                                               <div><span>"+album.description+"</span></div>\
                                                                           </a>\
                                                                           <p><h2>"+album.name+"</h2></p>\
                                                                        </li>";
                                                                		$('#gallery-thumbs').append(li);
                										            });
                                            });
                                        }else{
                                        	//alert(album.type);
                                            var li = "<li class='' style=\"width:48%;\">\
                                                       <a href='#gallery-"+album.id+"'>\
                                                           <img width='100%'src='"+album.thumb+"'/>\
                                                           <div><span>"+album.description+"</span></div>\
                                                       </a>\
                                                       <p><h2>"+album.name+"</h2></p>\
                                                    </li>";
                                            $('#gallery-thumbs').append(li);
                                        }
                                    });
                                    

                  var intvGallery = setInterval(function(){
								       	if($('#gallery-thumbs li').length == albums.length){
								       		             $('#main-content section').fadeIn(600);
		                                    $('html, body').animate({ scrollTop:0 }, 'fast');
		                                    //alert('gallery');
		                                    pageJS("jquery.hoverdir","grid3d","modernizr.custom","cbpGridGallery","masonry.pkgd.min");//for inpection purposes + add masonry
		                                    $('head').append('<link rel=\"stylesheet\" type=\"text/css\" href=\"css/gallery.css\">');
		                                    $(' #gallery-thumbs > li ').each( function() { $(this).hoverdir(); } );		                                   
		                                    galleryGrid = new grid3D( document.getElementById( 'sec-gallery' ) );
		                                    //alert('defined');
		                                    //console.log(galleryGrid);
								       		clearInterval(intvGallery);
								       	}else{
								       		console.log('gallery loading...');
								       	}
								       },100);
                                    
                                }
                         }); 
            
    }, "html" );
}
function getAlbum(album_id){
                    $.ajax( {
                    type: 'POST',
                    url: 'gallery-js.php',
                    data: 'type=album&id='+album_id,
                    dataType: 'json',
                    success: function(album) {
                        //console.log(album);
                        //var images;
                        if(album.type == 'facebook'){
                        	var details = {};
					       	var det_commas = album.description.split(',');
					       	det_commas.forEach(function(detail,det_key){
					            var det_colon = detail.split(':');
					            //console.log(det_colon);
					            details [det_colon[0]] = det_colon[1];
					        });
					        album['description'] = details.description;
					        //console.log(details.album_id);
	                        getFB(function(){
	                        //console.log(FB);
	                        var pageAccessToken = '1533598003587268|vAIK4Zjk5Yk5omwEsWBTnR26E4s';
	                            FB.api('/'+details.album_id+'/photos', 
								{
									access_token : pageAccessToken
								}, 
								function(response) {
									//make thumb grid
                            		var thumblist = $('#thumb-list');									
									thumblist.html("<li class=\"grid-sizer\"></li>");
									response.data.forEach(function(image,image_key){
                    var thumb_no = response.data[image_key].images.length-1;
										thumblist.append(
                                        "<li class=\"\" style=\"width:32%;\">\
                                              <a href=\"#gallery-"+(album_id)+"-"+(image_key+1)+"\">\
                                                <img width=\"100%\" src=\""+image.images[thumb_no].source+"\" />\
                                                <div><span>"+"KCC Image"+"</span></div>\
                                              </a>\
                                           </li>"); 
									});
									//make slideshow
		                            var imagelist = $('#image-list');
		                                imagelist.html('');
                                    //add function to get the best res photo, not too high, not too low
		                                response.data.forEach(function(image,vimage_key){
		                                        imagelist.append(
		                                            "<li>\
		                                                   <figure>\
		                                                        <figcaption>\
		                                                          <h3>Image Name</h3>\
		                                                          <p>Image description: "+"Description"+"</p>\
		                                                        </figcaption>\
		                                                        <img width=\"100%\" src=\""+image.images[0].source+"\" alt=\"img\"/>\
		                                                      </figure>\
		                                             </li>");   
		                             });	
							

								imgcount = response.data.length;
		                        });
							});
                        }else if(album.type == 'local'){
                            var images = album.images;
                            var thumblist = $('#thumb-list');
                            //make thumb grid
                            thumblist.html("<li class=\"grid-sizer\"></li>");
                            images.forEach(function(image,image_key){
                                thumblist.append(
                                        "<li style=\"width:32%;\" class=\"\">\
                                              <a href=\"#gallery-"+(album_id)+"-"+(image_key+1)+"\">\
                                                <img width=\"100%\" src=\""+image.path+"\" />\
                                                <div><span>"+image.information+"</span></div>\
                                              </a>\
                                           </li>"); 
                            });
                            //make slideshow
                            var imagelist = $('#image-list');
                                imagelist.html('');
                                images.forEach(function(image,vimage_key){
                                        imagelist.append(
                                            "<li>\
                                                   <figure>\
                                                        <figcaption>\
                                                          <h3>Image Name</h3>\
                                                          <p>Image description: "+image.information+"</p>\
                                                        </figcaption>\
                                                        <img width=\"100%\" src=\""+image.path+"\" alt=\"img\"/>\
                                                      </figure>\
                                                  </li>");   
                                        });
                                imgcount = images.length;
                        }

                    }
                    });         
				 var intvAlGrid = setInterval(function(){
                    if(galleryGrid && $('#thumb-list li').length>=imgcount ){
                        //console.log('finally');
                         galleryGrid._showContent(0);
                        $('.image-thumbs > li ').each(function () {
                            $(this).hoverdir();
                          });
                         cpbgallery = new CBPGridGallery(document.getElementById('gallery-views'));   
                        $('#thumb-list img').load(function() {
                            console.log('attempt resizing??');
                          //cpbgallery._initMasonry();
                        });
                        clearInterval(intvAlGrid); 
                    }else{
                        console.log('up '+($('#thumb-list li img').length)+' to '+imgcount );
                    }
                 },100);
}                        

