//global variable
var galleryGrid;
var cpbgallery;

//Main tabs
function navigateToHash(callback, oldCollective) {
    var collective = window.location.hash.substr(1).split('-');

    //stores the main tab in a variable
    var link = collective[0];

    //combines the hash with '-tab' to find the nav button
    var tab = '#' + link + '-tab';

    //in case of empty '#'s defaults to home
    if(link == "") {
        link = "home";
        tab = "#home-tab";
    }
    
    //deactivate the previous tabs and activate the new one
    $('#main-tabs>li.active').removeClass('active');
    $(tab).addClass('active');

    //clear the about dropdown of activity
    $('#about-dropdown>li.active').removeClass('active');

    //implement progress here bar later

    //contact tab is not a tab just modal
    if(collective[0] == "contact"){
        contactUs(true);
        return;
    }
    //map modal
    if(collective[0] == "map"){
        map(true);
        return;
    }
    //news is a special tab, and is handled separately
    if(collective[0] == "news") {
        newsTab(collective);
        return;
    }
    if(collective[0] == "articles") {
        articlesTab(collective);
        return;
    }
    if(collective[0] == "gallery") {
        galleryTabJS("other",collective);
        return;
    }
    
    //login, likewise has its own ajax function
    if($.inArray(collective[0], ["admin", "login", "logout"]) !== -1) {
        
        loginTab(collective);
        return;
    }
    method = false;

    //make ajax request
    $.ajax( {
                type: 'POST',
                url: 'load.php',
                data: 'page=' + link,
                dataType: 'html',
                success: function(data) {
                    //replaces the main-content div with new section
                    $(window).scrollTop(0)
                    $('#main-content').html(data);
                    $('#main-content section').hide();

                    //activate link
                    $('#main-header .nav_item_link').removeClass('nav_link_active');
                    $('#main-header a[href="#' + link + '"]').addClass('nav_link_active');


                    onLoaded = function() {
                        //fade in new div //and call secondaryTab in case
                        //removed --> $('#main-content section').fadeIn(600, secondaryTab(collective));
                        $('#main-content section').fadeIn(600);
                         //initialise boxesFX if its the home page
                        if(link == 'home') {
                            pageJS("modernizr.custom","boxesFx");
                            $('#main-header a[href="#"]').addClass('nav_link_active');
                            new BoxesFx( document.getElementById( 'boxgallery' ) );
                        }else if (link.substr(0, 5) == 'about') {
                            pageJS();
                            initiateAnchors();
                        }else{
                            pageJS();
                        }
                        //secondary tabs
                        callback();
                        //alert('loaded');
                    };


                    //check that images have loaded
                    var total = $('#main-content img').length;
                    if(total==0){
                        //for pages with no images
                        onLoaded();
                    }else if(link == 'home') {
                        $('#main-content #boxgallery img').load(function() {
                            //show page when first carousel image loads 
                                 $('img').unbind('load');
                                onLoaded();
                        }); 
                    }else{
                        var count = 0;
                        $('#main-content img').load(function() {
                            count++;
                            if(count >= total) {
                                onLoaded();
                            }
                        }); 
                    }
                
            }
    });
   
}
//loads javascript for individual pages
function pageJS(/**/){
    var files =  Array.prototype.slice.call(arguments);
    if(files == null )
    {
        $('#page-js').html('');
        return;
    }
    $('#page-js').html('');
    
    files.forEach(function (file,index){
        $('#page-js').append("<script src=\"js/"+file+".js\" type=\"text/javascript\"></script>");
        console.log("pagejs-> " + file);
    });
    /*for(index = 0; index < files.length; index++ ){
        $('#page-js').append("<script src=\"js/"+files[index]+".js\" type=\"text/javascript\"></script>");
    }*/
    /*
    for (file of arguments){ //did not work in firefox
        $('#page-js').append("<script src=\"js/"+file+".js\" type=\"text/javascript\"></script>");
    }*/
}
//navigates to hash as soon as site is loaded (no change triggered)
$(function () {
    var newURL = location.hash;
    var newCollective = newURL.substr(1).split('-');
    navigateToHash(function() {
        secondaryTab(newCollective, '');
    });


})


//cleans hash-ed urls of '#'
function getHashFromURL(url) {
    return url.substr(url.indexOf('#'));
}

function newTab(newCollective, oldCollective)
{

    var first = newCollective[0];
    var second = oldCollective[0];


    return (first !== second);
}

$('a[href="#contact"]').click(function() {
    contactUs();
    return false;
});

//triggered whenever hash changes
$(window).bind('hashchange', function(e) {
    //these lines simply ensure that the page changes only if the main
    //tab has changed, otherwise redirects directly to secondaryTab
    var oldURL = getHashFromURL(e.originalEvent.oldURL);
    var newURL = getHashFromURL(e.originalEvent.newURL);
    var oldCollective = oldURL.substr(1).split('-');
    var newCollective = newURL.substr(1).split('-');
    if(newTab(newCollective, oldCollective)) {
        navigateToHash(function() { 
            secondaryTab(newCollective, oldCollective);
        },oldCollective);
        return;
    }
    secondaryTab(newCollective, oldCollective);
    
});


//======================================================================================================
//Secondary tabs
function secondaryTab(newCollective, oldCollective) {
    //redirect to hash for news
    if(newCollective[0] === "news") {
        advanceNews(oldCollective, newCollective);
    }
    if(newCollective[0] === "articles") {
        articlesTab(newCollective);
    }
    if(newCollective[0] === "about") {
        if(newCollective[1] != undefined) {
            aboutTab(newCollective);
        }
    }
    if(newCollective[0] === "admin") {
        adminTab(newCollective[1]);
    }
    if(newCollective[0] === "sermons") {
        sermonTab(newCollective[1]);
    }
    if(newCollective[0] === "gallery") {
        galleryTabJS(oldCollective,newCollective);
    }
}

function sermonTab(sermon) {
    //quit if no sermon
    if(sermon == undefined) {
        return false;
    }
    
    $.ajax({
        type: 'POST',
        url: 'load.php',
        data: 'sermon=' + sermon,
        dataType: 'html',
        success: function(data) {
            $('div.focus').hide().html(data).fadeIn();
            $('.sermon-item').removeClass('active');
            $('#sermon-' + sermon).addClass('active');
        }   
    });  

}

function contactUs(oldCollective){
    $.ajax({
        type: 'POST',
        url: 'load.php',
        data: 'page=contact',
        dataType: 'html',
        success: function(data) {
            //load home page if no page in background
            if(location.hash == "#contact") {
                location.hash = '#';
            }
            $('#page-modal').html(data);
            $('#contact-modal').modal('show');
            $('#contact-modal').off('hidden.bs.modal').on('hidden.bs.modal', function() {
                $('#contact-modal').remove();

            });
        }   
    });  
}       
function map(oldCollective){
    $.ajax({
        type: 'POST',
        url: 'load.php',
        data: 'page=map',
        dataType: 'html',
        success: function(data) {
            //load home page
            if(oldCollective) {
                location.hash = '#';
            }
            $('#page-modal').html(data);
            $('#map-modal').modal('show');
            $('#map-modal').off('hidden.bs.modal').on('hidden.bs.modal', function() {
                $('#map-modal').remove();

            });
        }   
    });  
}    

function changehashURL(newURL, title){
    if(title == null) {
        title = $('title').html();
    }
    newURL = String(newURL).replace(/,/g,'-');
    var state = {
        "canBeAnything": true
    };  
    history.pushState(state, title, location.pathname+'#'+newURL);
    //expect(history.state).toEqual(state);
}      
function aboutTab(collective) {
    //scroll to tab
    scrollToAnchor(collective[1]);
    method = true;

    //confirm tertiary
    if(collective[2] == undefined)
        return;

    //get section
    var section = $('.about-container section[name=' + collective[1] + ']');


    //confirm section set
    if(section == undefined)
        return;

    //get new content
    var content = section.find('.sections #' + collective[2]).html();

    //set bg image if available
    var image = section.find('.sections #' + collective[2]).attr('image');
    if (image == undefined){

    }else{
        content = "<img class='actual-image' src='"+image+"'>" + content;
    }

    //set new content
    section.find('.content > div.actual').html(content).hide().fadeIn();


    //set active classes
    section.find('nav a').removeClass('active');
    section.find('a[href="' + location.hash + '"]').addClass('active');

}


//handles forward/backward requests
function advanceNews(oldCollective, newCollective) {
    //decide whether to move forward, backward, or neither
    var forward = (newCollective[1] == 'next') ? true : ((newCollective[1] == 'previous') ? false : null);

    //stores previous page in a variable and allows for default
    var last = (oldCollective[1] === null) ? 0 : parseInt(oldCollective[1]); 
    //quit if neither forward/backward or if not from numeric index
    if(forward == null || !$.isNumeric(oldCollective[1])) {
        newsTab(newCollective);
        return;
    }
    
    //change the location to the correct position
    location.hash = 'news-page-' + (forward ? last + 1 : ((last > 0) ? last - 1 : 0));
}
function newsTab(collective){

    $('#main-header .nav_link_active').removeClass('nav_link_active');
    $('#main-header a[href="#news"]').addClass('nav_link_active');
    
    //redirect #news-page-0 to #news
    if(location.hash === "#news-page-0") {
        location.hash = "#news";
    }
    //pretend #news is #news-page-0
    if(collective[1] == null) {
        collective[1] = 'page';
        collective[2] = '0';
    }

    //stores page/story for indiviudal pages
    var type = collective[1];

    //default values for count & offset
    var offset = 0;
    var count = 5;

    //sets the offset if collective[1] is a number
    if(collective[1] != null) {
        offset = $.isNumeric(collective[2]) ? parseInt(collective[2]) : offset;
    }

    //decide count to be implemented

    //make ajax request
    $.ajax({
                type: 'POST',
                url: 'news.php',
                data: 'offset=' + offset + '&count=' + count + '&type=' + type ,
                dataType: 'html',
                success: function(data) {
                    //redirect for meaningless data
                    if(data.indexOf("news-title") == -1) {
                        location.hash = "news";
                        return;
                    }
                    //replaces the main-content div with new section
                    $('#main-content').html(data);
                    //fade in new div and call secondaryTab in case
                    $('#main-content section').hide();

                    //update the back/forth controls
                    var next = offset + 1;
                    var previous = offset - 1;
                    var minimum = (type === "story") ? 1 : 0;
                    var total = parseInt($('#news-total').text());
                    
                    if(!(offset > minimum)) {
                        previous = minimum;
                        $('a[href="#news-previous"]').hide();
                    }
                    
                    if(total) {
                        next = offset;
                        $('a[href="#news-next"]').hide();
                    }

                    if(type === "story") {
                        $('a[href="#news-previous"]').hide();
                        $('a[href="#news-next"]').hide();
                    }

                    //change all links for next/previous
                    $('a[href="#news-next"]').attr('href', '#news-' + type + '-' + next);
                    $('a[href="#news-previous"]').attr('href', '#news-' + type + '-' + previous);

                    //clean up urls
                    $('a[href="#news-page-0"]').attr('href', '#news');


                    $('#main-content section').fadeIn(600);
                    $('html, body').animate({ scrollTop:0 }, 'fast');
                    cleanURL();
                }
    });
 


}

function articlesTab(collective){

    $('#main-header .nav_link_active').removeClass('nav_link_active');
    $('#main-header a[href="#articles"]').addClass('nav_link_active');
    
    //redirect #articles-0 to #articles
    if(location.hash === "#articles-0") {
        location.hash = "#articles";
    }
    //check if specific article is requested
    if(collective[1] == null) {
        var type = 'home';

            //default values for count & offset
            var offset = 0;
            var count = 5;

            //sets the offset if collective[1] is a number
            if(collective[1] != null) {
                offset = $.isNumeric(collective[2]) ? parseInt(collective[2]) : offset;
            }

            //decide count to be implemented

            //make ajax request
            $.ajax({
                        type: 'POST',
                        url: 'articles.php',
                        data: 'type=home',
                        dataType: 'html',
                        success: function(data) {
                            
                            //replaces the main-content div with new section
                            $('#main-content').html(data);
                            //fade in new div and call secondaryTab in case
                            $('#main-content section').hide();

                            //update the back/forth controls
                            var next = offset + 1;
                            var previous = offset - 1;
                            var minimum = (type === "story") ? 1 : 0;
                            var total = parseInt($('#news-total').text());
                            
                            $('#main-content section').fadeIn(600);
                            $('html, body').animate({ scrollTop:0 }, 'fast');
                            //cleanURL();
                        }
            });
 
    }else{
        if(collective[1]=="news"){
            //if #articles-news redirect to prebuilt #news for now 
            location.hash = "#news";
        }else if(collective[1]){
            var id = collective[1];
            var type = "article";
            //make ajax request to article view
            $.ajax({
                        type: 'POST',
                        url: 'article.php',
                        data: 'id=' + id ,
                        dataType: 'html',
                        success: function(data) {
                            //alert(data);
                           //redirect for meaningless data
                            /*if(data.indexOf("articles-title") == -1) {
                                location.hash = "articles";
                                return;
                            }*/
                            //replaces the main-content div with new section
                            $('#main-content').html(data);
                            //fade in new div and call secondaryTab in case
                            $('#main-content section').hide();

                            $('#main-content section').fadeIn(600);
                            $('html, body').animate({ scrollTop:0 }, 'fast');
                        }
            });
        }
    }



}

function adminTab(tabname) {
    //defaults tab to leadership
    if(tabname == undefined) {
        location.hash = 'admin-story';
        return;
    }
    
    //change content for tab and show
    $('#admin-tablist a[href="#admin-' + tabname + '"]').tab('show');
}

function loginTab(collective) {
    $.ajax({
                type: 'POST',
                url: 'access.php',
                data: { page: collective[0] },
                dataType: 'json',
                success: function(data) {
                    //all good
                    if(data.status === 200) {
                        pageJS("login");
                        if(collective[0] !== data.page) {
                            location.hash = data.page;
                            return;
                        }
                        $('#main-content').html(data.html).hide().fadeIn(600, secondaryTab(collective)); 
                    }
                    else
                    {
                        //error
                    }
                }
    });
}

function galleryTabJS(oldcollective,collective){
    $.getScript( "js/gallery.js")
      .done(function( script, textStatus ) {
        //console.log( textStatus );
        //alert('wait');
        
        if(oldcollective=="other"){
            getGallery();    
        }

        if(collective[1]==undefined){
            return;
        }else if(collective[1]==0){
            location.hash = '#gallery';
            return;
        }else if(oldcollective[1]!=collective[1]){
            getAlbum(collective[1]);       
               
        }
        /*Check if photo selected, if yes, load it*/
        if(collective[2]==undefined){
            //alert("No photo");
            return;
        }else if(collective[2]!=oldcollective[2]){
            //what if they equal??
           //alert('toa picha ya \n '+collective[2]);
           var intvAlbum = setInterval(function(){
            if(cpbgallery){
                cpbgallery._openSlideshow(collective[2]-1);
                clearInterval(intvAlbum);
            }else{
                console.log('album is still loading');
            }
            },100);
        }
    
   });
}
function galleryTabphp(oldcollective,collective){
    if(oldcollective=="other"){

        $.ajax( {
                    type: 'POST',
                    url: 'gallery-php.php',
                    data: 'type=gallery',
                    dataType: 'html',
                    success: function(data) {
                        //replaces the main-content div with new section
                        $(window).scrollTop(0)
                        $('#main-content').html(data);
                        $('#main-content section').hide();
                        $('#main-content section').hide();

                        $('#main-content section').fadeIn(600);
                        $('html, body').animate({ scrollTop:0 }, 'fast');

                        //activate link
                        $('#main-header .nav_item_link').removeClass('nav_link_active');
                        $('#main-header a[href="#gallery"]').addClass('nav_link_active');

                            //alert('gallery');
                            pageJS("jquery.hoverdir","grid3d","cbpGridGallery","masonry.pkgd.min");//for inpection purposes + add masonry
                            $('head').append('<link rel=\"stylesheet\" type=\"text/css\" href=\"css/gallery.css\">');
                            $(function() {
                                $(' #gallery-thumbs > li ').each( function() { $(this).hoverdir(); } );
                             });
                            galleryGrid = new grid3D( document.getElementById( 'sec-gallery' ) );
                            //new CBPGridGallery( document.getElementById( 'gallery-views' ) );
                   }
                   });     
    }

    if(collective[1]==undefined){
        return;
    }else if(collective[1]==0){
        location.hash = '#gallery';
    }else if(oldcollective[1]!=collective[1]){
             $.ajax( {
                    type: 'POST',
                    url: 'gallery-php.php',
                    data: 'type=album&id='+collective[1],
                    dataType: 'html',
                    success: function(data) {
                        //alert('got album');
                        console.log(data);
                        $('#album-view').html(data);

                        galleryGrid._showContent(0);
                        $('.image-thumbs > li ').each(function () {
                            $(this).hoverdir();
                        });
                        cpbgallery = new CBPGridGallery(document.getElementById('gallery-views'));
                        /*Check if photo selected, if yes, load it*/
                        
                    }
                });  
            // alert('gallery ya \n ' + collective[1]);
            
    }
    if(collective[2]==undefined){
        //alert("No photo");
        return;
    }else if(collective[2]!=oldcollective[2]){
        //what if they equal??
       //alert('toa picha ya \n '+collective[2]);
       cpbgallery._openSlideshow(collective[2]-1);
    }
    
    
}


//======================================================================================================
//About dropdown
$('#about-tab').click(function() {
    $('#about-dropdown').css('display', 'none');
});

$('#about-tab').hover(function() {
    $('#about-dropdown').css('display', 'block');
}, function() {
    $('#about-dropdown').css('display', 'none');
});


function scrollToAnchor(aid){
    var distance = $("section[name='"+ aid +"']").offset().top - 54;
    if(method) {
        $('html,body').animate({scrollTop: distance},'650');
    }
    else {
        $(window).scrollTop(distance);
    }
}

function initiateAnchors() {
    //enable clicks
    $('.about-container > nav > a').click(function() {
        var hash = $(this).attr('href');
        if(location.hash == hash) {
            scrollToAnchor(hash.substr(1).split('-')[1]);
        }
    });

    //quick snap
    var segments = location.hash.split('-');
    if(segments[1] == null) {
        method = true;
    }

    var sections = $('section')
        , nav = $('.about-container > nav')
        , nav_height = nav.outerHeight();

    $(window).on('scroll', function () {
        var cur_pos = $(this).scrollTop();

        sections.each(function() {
            var top = $(this).offset().top - nav_height,
            bottom = top + $(this).outerHeight();
            var total = (7 * (bottom - top) ) / 10;
            var centre = (top + bottom) / 2;

            if (cur_pos >= (centre - total) && cur_pos <= (centre + total)) {
                //remove other classes
                nav.find('a').removeClass('active');

                //add class 
                $(this).addClass('active');

                //activate all alike links
                nav.find('a[href="#about-'+$(this).attr('name')+'"]').addClass('active');

                //hash pointing to this
                var hash = '#about-' + $(this).attr('name');
            }
        });

});
}
