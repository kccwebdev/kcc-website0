/**
 * cbpGridGallery.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
;( function( window ) {
	
	'use strict';

	var docElem = window.document.documentElement,
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = {
			transitions : Modernizr.csstransitions,
			support3d : Modernizr.csstransforms3d
		};

	function setTransform( elcbp, transformStr ) {
		elcbp.style.WebkitTransform = transformStr;
		elcbp.style.msTransform = transformStr;
		elcbp.style.transform = transformStr;
	}

	// from http://responsejs.com/labs/dimensions/
	function getViewportW() {
		var client = docElem['clientWidth'],
			inner = window['innerWidth'];
		
		if( client < inner )
			return inner;
		else
			return client;
	}

	function extend( a, b ) {
		for( var key in b ) { 
			if( b.hasOwnProperty( key ) ) {
				a[key] = b[key];
			}
		}
		return a;
	}

	function CBPGridGallery( elcbp, options ) {
		this.elcbp = elcbp;
		this.options = extend( {}, this.options );
  		extend( this.options, options );
  		this._init();
	}

	CBPGridGallery.prototype.options = {
	};

	CBPGridGallery.prototype._init = function() {
		// main grid
		this.grid = this.elcbp.querySelector( 'div.image-grid > ul.image-thumbs' );
		//this.grid = $('div.image-grid > ul.image-thumbs');
		// main grid items
		this.gridItems = [].slice.call( this.grid.querySelectorAll( 'li:not(.grid-sizer)' ) );
		// items total
		this.itemsCount = this.gridItems.length;
		// slideshow grid
		this.slideshow = this.elcbp.querySelector( 'section.slideshow > ul' );
		// slideshow grid items
		this.slideshowItems = [].slice.call( this.slideshow.children );
		// index of current slideshow item
		this.current = -1;
		// slideshow control buttons
		this.ctrlPrev = this.elcbp.querySelector( 'section.slideshow > nav > span.nav-prev' );
		this.ctrlNext = this.elcbp.querySelector( 'section.slideshow > nav > span.nav-next' );
		this.ctrlClose = this.elcbp.querySelector( 'section.slideshow > nav > span.nav-close' );
		// init masonry grid
		this._initMasonry();
		// init events
		this._initEvents();
	};

	CBPGridGallery.prototype._initMasonry = function() {
		var grid = this.grid;
		/*imagesLoaded( grid, function() {
			new Masonry( grid, {
				itemSelector: 'li',
				columnWidth: grid.querySelector( '.grid-sizer' )
			});
		});*/
	    //alert('wait -> Let Images Load');
		//new Masonry( grid, { itemSelector: 'li', columnWidth: grid.querySelector( '.grid-sizer' )});
	};

	CBPGridGallery.prototype._initEvents = function() {
		var selfcbp = this;

		// open the slideshow when clicking on the main grid items
		/*this.gridItems.forEach( function( item, idx ) {
			item.addEventListener( 'click', function() {
				selfcbp._openSlideshow( idx );
			} );
		} );*/

		// slideshow controls
		this.ctrlPrev.addEventListener( 'click', function() { selfcbp._navigate( 'prev' ); } );
		this.ctrlNext.addEventListener( 'click', function() { selfcbp._navigate( 'next' ); } );
		this.ctrlClose.addEventListener( 'click', function() { selfcbp._closeSlideshow(); } );

		// window resize
		window.addEventListener( 'resize', function() { selfcbp._resizeHandler(); } );

		// keyboard navigation events
		(function(){
			document.addEventListener( 'keydown', function( ev ) {
				if ( selfcbp.isSlideshowVisible ) {
					var keyCode = ev.keyCode || ev.which;

					switch (keyCode) {
						case 37:
							selfcbp._navigate( 'prev' );
							break;
						case 39:
							selfcbp._navigate( 'next' );
							break;
						case 27:
							selfcbp._closeSlideshow();
							break;
					}
				}
			} );
		})();

		// trick to prevent scrolling when slideshow is visible
		window.addEventListener( 'scroll', function() {
			if ( selfcbp.isSlideshowVisible ) {
				window.scrollTo( selfcbp.scrollPosition ? selfcbp.scrollPosition.x : 0, selfcbp.scrollPosition ? selfcbp.scrollPosition.y : 0 );
			}
			else {
				selfcbp.scrollPosition = { x : window.pageXOffset || docElem.scrollLeft, y : window.pageYOffset || docElem.scrollTop };
			}
		});
	};

	CBPGridGallery.prototype._openSlideshow = function( pos ) {
		this.isSlideshowVisible = true;
		this.current = pos;

		/*hide image grid controls*/
		//alert('hide album X');
		$('.close-album').hide(); 

		/*classie.addClass( this.elcbp, 'slideshow-open' );*/
		$(this.elcbp).addClass('slideshow-open');

		/* position slideshow items */

		// set viewport items (current, next and previous)
		this._setViewportItems();
		
		// add class "current" and "show" to currentItem
		/*classie.addClass( this.currentItem, 'current' );*/
		$(this.currentItem).addClass('current');

		/*classie.addClass( this.currentItem, 'show' );*/
		$(this.currentItem).addClass('show');

		// add class show to next and previous items
		// position previous item on the left side and the next item on the right side
		if( this.prevItem ) {
			/*classie.addClass( this.prevItem, 'show' );*/
			$(this.prevItem).addClass('show');

			var translateVal = Number( -1 * ( getViewportW() / 2 + this.prevItem.offsetWidth / 2 ) );
			setTransform( this.prevItem, support.support3d ? 'translate3d(' + translateVal + 'px, 0, -150px)' : 'translate(' + translateVal + 'px)' );
		}
		if( this.nextItem ) {
			/*classie.addClass( this.nextItem, 'show' );*/
			$(this.nextItem).addClass('show');
			var translateVal = Number( getViewportW() / 2 + this.nextItem.offsetWidth / 2 );
			setTransform( this.nextItem, support.support3d ? 'translate3d(' + translateVal + 'px, 0, -150px)' : 'translate(' + translateVal + 'px)' );
		}
	};

	CBPGridGallery.prototype._navigate = function( dir ) {
		if( this.isAnimating ) return;
		if( dir === 'next' && this.current === this.itemsCount - 1 ||  dir === 'prev' && this.current === 0  ) {
			this._closeSlideshow();
			return;
		}

		this.isAnimating = true;
		
		// reset viewport items
		this._setViewportItems();

		var selfcbp = this,
			itemWidth = this.currentItem.offsetWidth,
			// positions for the centered/current item, both the side items and the incoming ones
			transformLeftStr = support.support3d ? 'translate3d(-' + Number( getViewportW() / 2 + itemWidth / 2 ) + 'px, 0, -150px)' : 'translate(-' + Number( getViewportW() / 2 + itemWidth / 2 ) + 'px)',
			transformRightStr = support.support3d ? 'translate3d(' + Number( getViewportW() / 2 + itemWidth / 2 ) + 'px, 0, -150px)' : 'translate(' + Number( getViewportW() / 2 + itemWidth / 2 ) + 'px)',
			transformCenterStr = '', transformOutStr, transformIncomingStr,
			// incoming item
			incomingItem;

		if( dir === 'next' ) {
			transformOutStr = support.support3d ? 'translate3d( -' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px, 0, -150px )' : 'translate(-' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px)';
			transformIncomingStr = support.support3d ? 'translate3d( ' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px, 0, -150px )' : 'translate(' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px)';
		}
		else {
			transformOutStr = support.support3d ? 'translate3d( ' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px, 0, -150px )' : 'translate(' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px)';
			transformIncomingStr = support.support3d ? 'translate3d( -' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px, 0, -150px )' : 'translate(-' + Number( (getViewportW() * 2) / 2 + itemWidth / 2 ) + 'px)';
		}

		// remove class animatable from the slideshow grid (if it has already)
		/*
		classie.removeClass( selfcbp.slideshow, 'animatable' );*/
		$(selfcbp.slideshow).removeClass('animatable');

		if( dir === 'next' && this.current < this.itemsCount - 2 || dir === 'prev' && this.current > 1 ) {
			// we have an incoming item!
			incomingItem = this.slideshowItems[ dir === 'next' ? this.current + 2 : this.current - 2 ];
			setTransform( incomingItem, transformIncomingStr );
			/*classie.addClass( incomingItem, 'show' );*/
			$(incomingItem).addClass('show');
			//console.log(incomingItem);
		}

		var slide = function() {
			// add class animatable to the slideshow grid
			/*classie.addClass( selfcbp.slideshow, 'animatable' );*/
			$(selfcbp.slideshow).addClass('animatable');

			// overlays:
			/*classie.removeClass( selfcbp.currentItem, 'current' );*/
			$(selfcbp.currentItem).removeClass('current');
			var nextCurrent = dir === 'next' ? selfcbp.nextItem : selfcbp.prevItem;
			/*classie.addClass( nextCurrent, 'current' );*/
			$(nextCurrent).addClass('current');

			setTransform( selfcbp.currentItem, dir === 'next' ? transformLeftStr : transformRightStr );

			if( selfcbp.nextItem ) {
				setTransform( selfcbp.nextItem, dir === 'next' ? transformCenterStr : transformOutStr );
			}

			if( selfcbp.prevItem ) {
				setTransform( selfcbp.prevItem, dir === 'next' ? transformOutStr : transformCenterStr );
			}

			if( incomingItem ) {
				setTransform( incomingItem, dir === 'next' ? transformRightStr : transformLeftStr );
			}

			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName.indexOf( 'transform' ) === -1 ) return false;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}

				if( selfcbp.prevItem && dir === 'next' ) {
					/*classie.removeClass( selfcbp.prevItem, 'show' );*/
					$(selfcbp.prevItem).removeClass('show');
				}
				else if( selfcbp.nextItem && dir === 'prev' ) {
					/*classie.removeClass( selfcbp.nextItem, 'show' );*/
					$(selfcbp.nextItem).removeClass('show');
				}

				if( dir === 'next' ) {
					selfcbp.prevItem = selfcbp.currentItem;
					selfcbp.currentItem = selfcbp.nextItem;
					if( incomingItem ) {
						selfcbp.nextItem = incomingItem;
					}
				}
				else {
					selfcbp.nextItem = selfcbp.currentItem;
					selfcbp.currentItem = selfcbp.prevItem;
					if( incomingItem ) {
						selfcbp.prevItem = incomingItem;
					}
				}

				selfcbp.current = dir === 'next' ? selfcbp.current + 1 : selfcbp.current - 1;
				selfcbp.isAnimating = false;
			};

			if( support.transitions ) {
				selfcbp.currentItem.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		};

		setTimeout( slide, 25 );
	}

	CBPGridGallery.prototype._closeSlideshow = function( pos ) {
		//pos = pos -1;
		// remove class slideshow-open from the grid gallery elem
		/*classie.removeClass( this.elcbp, 'slideshow-open' );*/
		$(this.elcbp).removeClass('slideshow-open');
		// remove class animatable from the slideshow grid
		/*classie.removeClass( this.slideshow, 'animatable' );*/
		$(this.slideshow).removeClass('animatable');

		var selfcbp = this,
			onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.target.tagName.toLowerCase() !== 'ul' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				// remove classes show and current from the slideshow items
				/*classie.removeClass( selfcbp.currentItem, 'current' );*/
				$(selfcbp.currentItem).removeClass('current');
				//console.log(selfcbp.currentItem);
				/*classie.removeClass( selfcbp.currentItem, 'show' );*/
				$(selfcbp.currentItem).removeClass('show');
				//console.log(selfcbp.currentItem);
				//selfcbp.slideshowItems.forEach( function( item ) {$(item).removeClass('show');$(item).removeClass('current'); } );
				//alert('Close Slideshow');
				if( selfcbp.prevItem ) {
					//classie.removeClass( selfcbp.prevItem, 'show' );
					$(selfcbp.prevItem).removeClass('show');
				}
				if( selfcbp.nextItem ) {
					//classie.removeClass( selfcbp.nextItem, 'show' );
					$(selfcbp.nextItem).removeClass('show');
				}

				// also reset any transforms for all the items
				selfcbp.slideshowItems.forEach( function( item ) { setTransform( item, '' ); } );

				selfcbp.isSlideshowVisible = false;
				var newURL = window.location.hash.substr(1).split('-');
				changehashURL('gallery-'+newURL[1],null);/*get position var*/
				//alert('show album X');
				$('.close-album').show(); 

			};

		if( support.transitions ) {
			this.elcbp.addEventListener( transEndEventName, onEndTransitionFn );
		}
		else {
			onEndTransitionFn();
		}

	};

	CBPGridGallery.prototype._setViewportItems = function() {
		this.currentItem = null;
		this.prevItem = null;
		this.nextItem = null;

		if( this.current > 0 ) {
			this.prevItem = this.slideshowItems[ this.current - 1 ];
		}
		if( this.current < this.itemsCount - 1 ) {
			this.nextItem = this.slideshowItems[ this.current + 1 ];
		}
		this.currentItem = this.slideshowItems[ this.current ];
	}

	// taken from https://github.com/desandro/vanilla-masonry/blob/master/masonry.js by David DeSandro
	// original debounce by John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	CBPGridGallery.prototype._resizeHandler = function() {
		var selfcbp = this;
		function delayed() {
			selfcbp._resize();
			selfcbp._resizeTimeout = null;
		}
		if ( this._resizeTimeout ) {
			clearTimeout( this._resizeTimeout );
		}
		this._resizeTimeout = setTimeout( delayed, 50 );
	}

	CBPGridGallery.prototype._resize = function() {

		if ( this.isSlideshowVisible ) {
			// update width value
			if( this.prevItem ) {
				var translateVal = Number( -1 * ( getViewportW() / 2 + this.prevItem.offsetWidth / 2 ) );
				setTransform( this.prevItem, support.support3d ? 'translate3d(' + translateVal + 'px, 0, -150px)' : 'translate(' + translateVal + 'px)' );
			}
			if( this.nextItem ) {
				var translateVal = Number( getViewportW() / 2 + this.nextItem.offsetWidth / 2 );
				setTransform( this.nextItem, support.support3d ? 'translate3d(' + translateVal + 'px, 0, -150px)' : 'translate(' + translateVal + 'px)' );
			}
		}
	}

	// add to global namespace
	window.CBPGridGallery = CBPGridGallery;

})( window );