<?
    //build an item
    function generateSermon($id)
    {
        //get the list of items from the database
        $item = query('SELECT a.title, a.scripture, b.name AS series, CONCAT(d.firstname, \' \', d.lastname) AS speaker, f.path AS audio, g.path AS video, h.path AS slides, c.path AS picture, e.date FROM `sermons` AS a
                            LEFT JOIN `services` e
                                ON a.id = e.sermon
                            INNER JOIN `series` AS b
                                ON a.series = b.id
                            INNER JOIN `people` AS d
                                ON a.speaker = d.id
                            LEFT JOIN `media` AS c
                                ON b.picture = c.id
                            LEFT JOIN `media` AS f
                                ON a.audio = f.id
                            LEFT JOIN `media` AS g
                                ON a.video = g.id
                            LEFT JOIN `media` AS h
                                ON a.slides = h.id
                            WHERE a.id = ?', $id);

        //exit if item doesn't exits
        if(!(isset($item[0]) && $item = $item[0]))
        {
            return;
        }

        //get item into memory
        extract($item);

        ?>
            <div class="player">
                
                <iframe id="youtube_player" type="text/html" width="100%" height="100%" frameborder="0"
                src="http://www.youtube.com/embed/<?= isset($video) ? $video : '' ?>?origin=http://karencommunitychurch.org"/>
            </div>
            <h1 class="text-capitalize"><?= $title ?></h1>
            <p class="controls">
                <div style="padding-top:2%;float:left;">
                    <?include(VIEWS_PATH . "share.php");?>
                    
                </div>
              <!--  <?/*= isset($video) ? "<a href=\"#play\">" . glyphicon("play-circle") . "</a>" : '' ?>
                <?= isset($audio) ? "<a href=\"$audio\" download>" . glyphicon("headphones") . "</a>" : '' ?>
                <?= isset($video) ? "<a href=\"$video\" download>" . glyphicon("download") . "</a>" : '' */?>-->
            </p>
            <p class="details">
                <?= "by <strong>$speaker</strong>" ?>
            </p>
        <?
    }

    //generate the items in the sermon list
    function generateSermonList()
    {
        //get the list of items from the database
        $items = query('SELECT a.id, a.title, CONCAT(d.firstname, \' \', d.lastname) AS speaker,d.title AS speakertitle, c.path AS picture, e.date AS date FROM `sermons` AS a 
                            LEFT JOIN `services` as e
                                ON a.id = e.sermon
                            INNER JOIN `series` AS b
                                ON a.series = b.id
                            INNER JOIN `people` AS d
                                ON a.speaker = d.id
                            LEFT JOIN `media` AS c
                                ON b.picture = c.id
                            ORDER BY e.date DESC');

        //iterate through items 
        foreach($items as $sermon)
        {
            //extract sermon into memory
            extract($sermon);
            //date_create_from_format('Y-m-d', $date);
            $date = strtotime($date);
            //build the item in the list
            ?>
                <a class="sermon-link" href="#sermons-<?= $id ?>">
                    <div class="sermon-item untouchable" id="sermon-<?= $id ?>">
                        <img src="<?=isset($picture) ? $picture . '?' . time() :'' ?>" width="90" height="80">
                        <div class="text">
                            <p class="date"><?=date('l jS F Y',$date) ?></p>
                            <p class="title text-capitalize"><?= $title ?></p>
                            <p class="speaker">by: <?echo(isset($speakertitle)?$speakertitle:'');?><?=' '.$speaker?></p>
                        </div>
                    </div>
                </a>
            <?
        }

    }

?>
