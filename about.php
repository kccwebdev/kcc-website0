<?php

    //THIS PAGE NEEDS SERIOUS REVISION

    //prepare the example text
    $example = query("SELECT `information` FROM `info` WHERE `name`='example'");
    $example=$example[0]["information"];
    
    $mission = query("SELECT `information` FROM `info` WHERE `name`='mission'");
    $mission=$mission[0]["information"]; 
    $vision = query("SELECT `information` FROM `info` WHERE `name`='vision'");
    $vision=$vision[0]["information"];
    $beliefs = query("SELECT `information` FROM `info` WHERE `name`='statement_of_faith'");
    $beliefs=$beliefs[0]["information"];
    $history = query("SELECT `information` FROM `info` WHERE `name`='history'");
    $history=$history[0]["information"];
	
	$adults = query("SELECT `information` FROM `info` WHERE `name`='adults'");
	$adults=$adults[0]["information"];
	$youth = query("SELECT `information` FROM `info` WHERE `name`='youth'");
	$youth=$youth[0]["information"];
	$children = query("SELECT `information` FROM `info` WHERE `name`='children'");
	$children=$children[0]["information"];
  
    $images = query("SELECT m.type,m.path FROM `galleries` AS g
            INNER JOIN `media_items` AS i
                ON g.id = i.gallery
            INNER JOIN `media` AS m
                ON i.item = m.id
            WHERE g.name = 'about'");
    foreach ($images as $key => $image) {
        # put all images in a single asc array
        $image_paths[$image['type']] = $image['path'];
    }
    //echo "<pre>"; print_r($image_paths); echo "</pre>"; 

    $structure = $example;
    $mission1 = $mission2 = $mission3 = "";

	function getProfiles($group)
	{
        //get list of pastors
        $pastors = query("SELECT CONCAT(p.firstname, ' ', IFNULL(CONCAT(SUBSTRING(p.middlename, 1, 1), '. '), ''), p.lastname) AS name,
                            p.bio, p.title, m.path AS photo
                            FROM `people_lists` AS l
                                INNER JOIN `people` AS p
                                    ON l.member = p.id
                                INNER JOIN `media` AS m
                                    ON m.id = p.photo
                                INNER JOIN `groups` AS g
                                    ON l.group = g.id
                                    AND g.name = ?", $group);
        //helps determine side
        $count = 0;

        //iterate through pastors, printing designs
        foreach($pastors as $pastor)
        {
            //extract pastor into memory
            extract($pastor);

            //increment count
            $count++;
            
            //ascertain side
            $side = ($count % 2 == 0) ? "left" : "right";

            //prepare profile
            include(VIEWS_PATH . "leader.php");
        }
	}
?>
