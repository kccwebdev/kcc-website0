<?

    require_once("includes/helpers.php");

    //destroy all session variables
    $_SESSION = array();
    
    //clear cookies
    if (ini_get("session.use_cookies")) 
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]);
    }

    //destroy the session
    session_destroy();

    //restart session
    session_start();

    //call login after clearing post
    $_POST = array();
    require("login.php");

    //exit for good measure
    exit();

?>

