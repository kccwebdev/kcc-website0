<div class="panel panel-normal">
    <div id="news-page" class="panel-body">

        <!-- Get news items -->
        <? 
            foreach($items as $item)
            {
                extract($item);
                ?>
                <!-- <?=$name?> -->
                <a id="news-<?=$id?>"></a>
                <div class="page-header news-header">
                    <h1 class="news-title"><?=$name?> <small class="news-text"><?=date("jS F Y", strtotime($startdate))?></small></h1>
                </div>

                <p class="news-text"><?=escape($information)?></p>
                <!-- End <?=$name?> -->
                <br><br>
                <?
               
            }
            
         print("<span class='passed-data' id='news-total'>$hide</span>");
            
         ?>
        <!-- End news items -->

        <ul class="pager">
            <li id="news-control-previous" class="position-control previous"><a href="#news-previous">&larr;</a></li>
            <li id="news-control-next" class="position-control next"><a href="#news-next">&rarr;</a></li>
        </ul>

    </div>
</div><!-- News Scope -->
