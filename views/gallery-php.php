<!-- <script type="text/javascript" src="js/masonry.pkgd.min.js"></script> -->
<section id="sec-gallery" class="sec-gallery grid3d vertical">
  <div id="grid-wrap" class="grid-wrap">
		<div class="gallery-grid">
		    <ul id="gallery-thumbs" class="gallery-thumbs da-thumbs">
		<?foreach ($gallery as $key => $album) 
			{
			# code...
				# show only if gallery has images
		?>
				<li class="">
				   <a href="#gallery-<?=$album['id']?>">
				       <img src="<?=$album['thumb']?>"/>
				       <div><span><?=$album['description']?></span></div>
				   </a>
			       <p><h2><?=$album['name']?></h2></p>
			    </li>
		<?
		}
		?>
			</ul>
		 </div>
	</div>
	<div id="gallery-views" class="gallery-views">	
		<div id = "album-view">
			<!--<div class="image-grid">
				image thumbs
			</div>-->
			<!--<section id="images-view" class="slideshow">
				images
			</section>-->
		</div>
		<span class="loading"></span>
		<span class="icon close-album glyphicon glyphicon-remove"></span>
	</div>
</section>
  
    <noscript><link rel="stylesheet" type="text/css" href="css/noJS.css"/></noscript>