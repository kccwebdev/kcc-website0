<div class="panel panel-normal">
    <div id="updates-page" class="panel-body">
        <div class="row">
            <h1 class="heading">LATEST UPDATES</h1>
            <div class="col-md-12">
                <div id="news-items-panel">
                    <ul class="cbp_tmtimeline">
                    <? foreach ($news as $item) { ?>
                    <li>
                        <time class="cbp_tmtime" datetime="<?= $item['startdate'] ?>">
                            <span><?= getDateFromRaw($item['startdate']) ?></span>
                            <span><?= getTimeFromRaw($item['startdate']) ?></span>
                        </time>
                        <div class="cbp_tmicon"></div> 
                        <div class="cbp_tmlabel">
                            <h2><?= $item['name'] ?></h2>
                            <p><? echo $item['information']; ?></p>
                        </div>
                    </li>
                    <? } ?>
                    </ul>
                </div>
            </div>
        </div>
        <a name="updates-articles"></a>
        <div class="row">
            <h1 class="heading">READ ARTICLES</h1>
            <div class="col-md-12">
                <? foreach ($articles as $item) { ?>
                <div class="article">
                    <h2><a href="#articles-<?= $item['id'] ?>"><?= $item['name'] ?></a></h2>
                    <h4><small><?= getDateFromRaw($item['date_published'], 'F j, Y') ?></small></h4>
                    <p><?= $item['preview'] ?></p>
                </div>
                <? } ?>
            </div>
        </div>
    </div>
</div><!-- Updates Scope -->
