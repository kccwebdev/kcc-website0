<?php
  
    include_once (CONTROLS_PATH."about.php");

?>

<div class="about-container">
    <div id="about-sections">
        <section class="light" id="about-story-section" name="story">
            <div class="content">
                <h3>Who We Are</h3>
                <div class="actual"><h2>Our History</h2><?= $history ?></div>
                <nav>
                    <a href="#about-story-mission"><span>Our Mission</span></a>
                    <a href="#about-story-vision"><span>Our Vision</span></a>
                    <a href="#about-story-beliefs"><span>Our Beliefs</span></a>
                    <a class="active" href="#about-story-history"><span>Our History</span></a>
                </nav>
            </div>
            <div class="sections">
                <div id="mission"><h2>Our Mission</h2><?= $mission ?></div>
                <div id="vision"><h2>Our Vision</h2><?= $vision ?></div>
                <div id="beliefs"><h2>Our Beliefs</h2><?= $beliefs ?></div>
                <div id="history"><h2>Our History</h2><?= $history ?></div>
            </div>
        </section>
        <section class="dark" id="about-ministries-section" name="ministries">
            <div class="content">
                <h3>Our Ministries</h3>
                <div class="actual"><img class='actual-image' src='<?=$image_paths['min_adults']?>'><h2>Adult Ministry</h2><?= $adults ?></div>
                <nav>
                    <a class="active" href="#about-ministries-adults"><span>Adult Ministry</span></a>
                    <a href="#about-ministries-youth"><span>Ground Zero</span></a>
                    <a href="#about-ministries-children"><span>Children's Ministry</span></a>
                </nav>
            </div>
            <div class="sections">
                <div image="<?=$image_paths['min_adults']?>" id="adults"><h2>Adult Ministry</h2><?= $adults ?></div>
                <div image="<?=$image_paths['min_youth']?>" id="youth"><h2>Youth Ministry (gZ)</h2><?= $youth ?></div>
                <div image="<?=$image_paths['min_children']?>" id="children"><h2>Children's Ministry</h2><?= $children ?></div>
            </div>
        </section>
        <section class="light" id="about-leadership-section" name="leadership">
            <div class="content">
                <h3>Our Leadership</h3>
                <div class="actual"><h2>Pastors</h2><? getProfiles('pastors'); ?></div>
                <nav>
                    <!-- <a class="active" href="#about-leadership-structure"><span>Leadership Structure</span></a> -->
                    <a class="active" href="#about-leadership-pastors"><span>Pastoral Team</span></a>
                    <a href="#about-leadership-elders"><span>Elders</span></a>
                </nav>
            </div>
            <div class="sections">
                <!-- <div id="structure"><?= $structure ?></div> -->
                <div id="pastors"><h2>Pastors</h2><? getProfiles('pastors'); ?></div>
                <div id="elders"><h2>Elders</h2><? getProfiles('elders'); ?></div>
            </div>
        </section>
        <section class="dark" id="about-missions-section" name="missions">
            <div class="content">
                <h3>Our Missions</h3>
                <div class="actual"><?= $mission1 ?></div>
                <nav>
                    <a class="active" href="#about-missions-mission1"><span>Mission 1</span></a>
                    <a href="#about-missions-mission2"><span>Mission 2</span></a>
                    <a href="#about-missions-mission3"><span>Mission 3</span></a>
                </nav>
            </div>
            <div class="sections">
            </div>
        </section>
        <section class="light" name="services">
            <div class="content">
                <h3>Our Services</h3>
                <div class="actual">
                    <h2>Our sunday service</h2>
                    <h2>Service times:- 10:00AM to 12:00PM every Sunday. </h2>
                    <p>We have three simultaneous services each Sunday ie. <ul><li>Adult Service</li>
                                                                               <li>Youth Service</li>
                                                                               <li>Children's Service</li></ul>
                       Each service uniquely run to fit the given age group.  </p>
                       <br><a onclick="map()" id="locationLauncher">Church Location</a><br>
            </div>
            </div>
            <div class="sections">
            </div>
        </section>
    </div>
    <nav class="unfixed">
        <a href="#about-story"></a>
        <a href="#about-ministries"></a>
        <a href="#about-leadership"></a>
        <a href="#about-missions"></a>
        <a href="#about-services"></a>
    </nav>
</div>
