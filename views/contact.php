<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
    <div class="panel panel-normal" id="contact-panel">
    <div id="contact-body" class="panel-body text-left form-normal">
        <button type="button" id="#modal-close" class="btn pull-right" data-dismiss="modal">
                <?= glyphicon("remove-circle") ?>
            </button>
        <h1>Contact KCC </h1>    
        <p><?=$contact_message?></p>
        <br>
        <address>
            <strong>Karen Community Church</strong><br>
            Email: <span><a href="mailto:<?=$church_email?>" itemprop="email"><?=$church_email?></a></span><br>
            Phone: <span><a href="tel:<?=$church_numbers[0]?>" itemprop="telephone"><?=$church_numbers[0]?></a><a href="tel:<?=$church_numbers[1]?>" itemprop="telephone"><?=$church_numbers[1]?></a></span><br>
        </address>

        <!-- Contact box -->
        <form role="form">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="name" class="form-control" id="name" placeholder="Enter name">
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control contact-message" id="message" rows="5" placeholder="Enter message"></textarea>
            </div>
            <button type="submit" class="btn btn-default">Submit Message</button>
        </form> 
        <br> 
        <p><?=$complete_message?></p>
        <a href="https://twitter.com/intent/tweet?screen_name=karen_community" class="twitter-mention-button" data-related="karen_community">Tweet to @karen_community</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

    </div>
</div><!-- Contact Scope -->
      </div>
    </div>
  </div>
</div>