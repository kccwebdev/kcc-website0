<div class="article-view" id="article-<?= $data['id'] ?>">
    <h1 id="article-title"><?=$data['name']?></h1>
    <p>Published on <?=$date?><?=(isset($publisher) ? ' by '.$publisher : '')?></p>
    <hr>
    <? showNewsItem($_POST["id"]);?>
    <div id="article-data">
    	<p><?=$data['data']?></p>
    </div>
    <a href="#updates-articles">Back to all articles</a>
    <?include(VIEWS_PATH . "share.php");?>
</div>

