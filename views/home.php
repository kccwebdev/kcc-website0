<? include_once(CONTROLS_PATH."home.php"); ?>
<!-- IMAGE CAROUSEL -->
<div id="boxwrap" >
    <div id="boxgallery" class="boxgallery untouchable" data-effect="effect-3">
        <? generateCarouselItems() ?>
    </div>
    <button class="trigger" data-info="Click to see the header effect"><span>Trigger</span></button>
</div>


<!-- CONTENT LINKS -->
<div id="content-links" class="container-fluid untouchable">
    <figure>
        <img src="imgs/links/about.jpg">
        <figcaption class="untouchable">
            <h2><span>About</span> Us</h2>
            <p><a href="#about-story">Who We Are</a></p><br>
            <p><a href="#about-ministries">Our Ministries</a></p><br>
            <p><a href="#about-missions">Our Missions</a></p><br>
            <a href="#about">View more</a>
        </figcaption>
    </figure>
    <figure>
        <img src="imgs/links/sermons.jpg">
        <figcaption>
            <h2>Our <span>Sermons</span></h2>
            <p><a href="#sermons-<?= $last ?>">Last Sunday</a></p><br>
            <p><a href="#sermons">Browse Sermons</a></p><br>
            <a href="#sermons">View more</a>
        </figcaption>
    </figure>
    <figure>
        <img src="imgs/links/services.jpg">
        <figcaption>
            <h2><span>Attend</span> Services</h2>
            <p><a href="#about-services">Service Times</a></p><br>
            <p ><a onclick="map()">Church Location</a></p><br>
            <a href="#about-services">View more</a>
        </figcaption>
    </figure>
    <figure>
        <img src="imgs/links/gallery.jpg<?= '?' . time() ?>">
        <figcaption>
            <h2>View <span>Gallery</span></h2>
            <a href="#gallery">View more</a>
        </figcaption>
    </figure>
</div>

<div id="news-stream" class="container-fluid untouchable">
    <div id="news-items">
        <? getNewsItems(); ?>
    </div>
</div>
