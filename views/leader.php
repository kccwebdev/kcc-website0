<div class="leader-profile <?= $side ?>">
    <img class="photo" src='<?= $photo ?>'></img>
    <h4><?= $name ?></h4>
    <h5><?= $title ?></h5>
    <p><?= $bio ?></p>
</div>
