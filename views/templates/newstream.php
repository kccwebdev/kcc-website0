<div class="">
    <div class="panel panel-primary" id="newstream-container">
        <div class="panel-heading">
            <h2 class="panel-title">
                <b>Latest KCC news...</b>
           </h2>
        </div>
        <div class="panel-body">
            <div class="panel-group" id="newstream">
                
            <? $primero = true; 

                $news_list = query("SELECT * FROM `news_items` WHERE `expirydate` > '" . date("Y-m-d G:i:s") . "' ORDER BY `startdate` DESC LIMIT 5"); 

                foreach($news_list as $item)
                {
                    extract($item); 
                    $short = prepareStringForHTML($name);
                    $long = substr($information, 0, 50);
            ?>
                    <!-- <?=$name?> -->
                    <div class="panel panel-news">
                        <div class="panel-heading panel-heading-<?=$category?>" data-toggle="collapse" data-parent="#newstream" href="#<?=$short?>">
                            <h4 class="panel-title" ><?=$name?></h4>
                        </div>
                        <div id="<?=$short?>" class="panel-collapse collapse<?= $primero ? " in" : ""?>">
                            <div class="panel-body">
                                <?=$long?> <a href="#news-story-<?=$id?>">Read more...</a>
                            </div>
                        </div>
                    </div>

    
                <?
                    $primero = false;
                } 

                ?>

            </div>
        </div>
    </div>
</div> <!-- end Newstream -->


