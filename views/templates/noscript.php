<noscript>
    <style type="text/css">
        #page-container{
            display: none;
        }

        #header {
            display: none;
        }
    </style>
    <div class="jumbotron">
        <h1>We have a problem...</h1>
        <h3>You need javascript to view this site.</h3>
        <h5>
            You can enable it from your browser settings.
            Click <a href="/">here</a> to reload when you're done.
        </h5> 
    </div>
</noscript>
