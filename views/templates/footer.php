                        </div> <!-- end main-content -->
                    </div> <!-- end main row -->
                </div> <!--end page-container-->
             </div> <!--end wrapper-->
             <div id="page-modal"></div>
        </div> <!-- end page-container -->

        <!-- Footer Panel-->            
        <footer>
            <div class="container footer">
                <div class="col-xs-12 col-md-8 noPadding">
                    <p id="footerLogoText">Karen Community Church © 2015</p>
                    <!-- <nav id="foot-nav" role="navigation" class="">
                        <ul class="menu js-menu vanilla">
                            <li class="nav_item nav_item_active">
                                <a href="#" class="nav_item_link js-link">
                                    <span class="nav_item_span">Home</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#about" class="nav_item_link js-link ">
                                    <span class="nav_item_span">About</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#sermons" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Sermons</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#gallery" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Gallery</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#contact" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Contact</span>
                                </a>
                            </li>
                    </nav> -->
                </div>
                <div class="col-xs-12 col-md-4 noPadding">
                    <div class="socialButtons responsiveAlign">
                        <a href="https://twitter.com/karen_community" target="_blank"  class="noPadding">
                            <img src="imgs/icons/twitter.png" alt="twitter">
                        </a>
                        <a href="https://www.facebook.com/pages/Karen-Community-Church/776188339163814" target="_blank" class="noPadding">
                            <img src="imgs/icons/facebook.png" alt="facebook">
                        </a>
                    </div>
                   <!-- <div>
                        <a href="https://twitter.com/karen_community" class="twitter-follow-button" data-show-count="false">Follow @karen_community</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        <div class="fb-like" data-href="https://www.facebook.com/pages/Karen-Community-Church/776188339163814" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div>
                    </div>-->
                </div>
            </div>
        </footer>
        
        <!-- Site javascript libraries here to save loading time -->
       
        <!-- jQuery --> 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="js/jquery.min.js"><\/script>')</script>

        <!-- Modernizr 
        <script src="js/modernizr.custom.js" type="text/javascript"></script>
        -->
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Custom JS -->
        <script src="js/tabs.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/admin.js"></script>
        <div id="page-js"></div>
    </body>
</html>
