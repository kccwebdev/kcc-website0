<!DOCTYPE html>
<html lang="en" class="no-js">  
    <head>
        <!-- Metadata -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial scale=1">

        <!-- Title -->
        <title><?=$title?></title>

        <!-- Icon -->
        <link rel="icon" type="image/png" href="imgs/banner/logo.ico">

         <!-- Bootstrap -->
        <!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- JQUERY UI -->
        <link href="css/jquery-ui.min.css" rel="stylesheet">

        <!-- Normalize -->
        <link href="css/normalize.css">

         <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
        <link href="css/tabs.css" rel="stylesheet">
        <link href="css/boxes.css" rel="stylesheet">
        
    </head>
    <body>
            <div class="" id="fb-root"></div>
            <div id="fb-root"></div>
           
            <!-- Header -->
           <header id="main-header" class="js-fixed untouchable">
            <div id="header-wrapper">
                <div id="header_content">
                    <a href="" id="logo-wrapper-link">
                        <div id="logo_wrapper">
                            <img id="logo" src="imgs/banner/logo.png" alt="MyLogo" height="auto" width="100">
                        </div>  
                        <div id="logo-text">
                            KAREN<br>
                            COMMUNITY<br>
                            CHURCH<br>
                        </div>
                    </a>
                    <nav id="main-nav" role="navigation" class="">
                        <ul class="menu js-menu vanilla">
                            <li class="nav_item nav_item_active">
                                <a href="#" class="nav_item_link js-link">
                                    <span class="nav_item_span">Home</span>
                                </a>
                            </li>
                            <li id="about-tab" class="nav_item">
                                <a href="#about" class="nav_item_link js-link ">
                                    <span class="nav_item_span">About</span>
                                </a>
                                <!--<ul id="about-dropdown" class="dropdown-menu" role="menu">
                                    <li id="about-story-tab"><a href="#about-story">Who We Are</a></li>
                                    <li id="about-ministries-tab"><a href="#about-ministries">Our Ministries</a></li>
                                    <li id="about-leadership-tab"><a href="#about-leadership">Our Leadership</a></li>
                                    <li id="about-missions-tab"><a href="#about-missions">Our Missions</a></li>
                                    <li id="about-services-tab"><a href="#about-services">Sunday Services</a></li>
                                   
                                </ul>-->
                            </li> 
                            <li class="nav_item">
                                <a href="#sermons" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Sermons</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#updates" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Updates</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#gallery" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Gallery</span>
                                </a>
                            </li>
                            <li class="nav_item">
                                <a href="#contact" class="nav_item_link js-link ">
                                    <span class="nav_item_span">Contact</span>
                                </a>
                            </li>
                    </nav>
                </div>
            </div>
        </header>
        <div class="wrapper" style="min-height: 64.2%;">
            <div class="push"></div>
        <div class="container mainfont" id="page-container">
            <div id="page-row" class="row">
                <div id="main-content">

                    
