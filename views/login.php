<div class="panel panel-normal" id="login-panel">
    <div id="login-body" class="panel-body text-left form-normal">
        <h1>Login to KCC</h1>    
        <div id="login-area" class="container-fluid">
            <p><?=$login_message?></p>

            <!-- Login form  -->
            <form id="login-form" onsubmit="loginSubmit(); return false;">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Enter password">
                </div>
                <button id="submit-button" type="submit" class="btn btn-default">Log in</button>
            </form> 
            <br> 
            <p><?=$complete_message?></p>
        </div>
    </div>
</div><!-- Contact Scope -->
