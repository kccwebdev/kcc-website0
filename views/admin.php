<?
$email = "markwahome@live.com";
print_r($data=query("SELECT * FROM `people` AS p INNER JOIN `users` AS u ON p.id = u.id WHERE `email` = '$email'"));
echo (count($data));
?>
<div class="panel panel-normal">
    <div class="panel-body">
        <div class="row centered" data-spy="scroll" data-target="#side-bar" style="position: relative;">

            <!-- Heading -->
            <h1>KCC Site Administration</h1>
            <h3>Welcome to the administration page, to edit the site, click on the tab of your choice.</h3>

            <!-- Navigation -->
            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked nav-tablist" id="admin-tablist">
                    <li class="active"><a href="#admin-home">Home</a>
                    <li><a href="#admin-services">Services</a>
                        <ul>
                          <li><a href="#admin-ministries-adults">Adult's Ministry</a></li>
                        </ul>
                    </li>
                    <li><a href="#admin-news">News/Events</a>
                        <ul>
                          <li><a href="#admin-news-add">Add New Event</a></li>
                        </ul>
                    </li>
                    <li><a href="#admin-members">Members</a></li>

                    <li><a href="#admin-message">Send out message</a></li>
                    <li><a href="#admin-about">About</a></li>
                        <ul>
                          <li><a href="#admin-about-story">Who we are</a></li>
                          <li><a href="#admin-about-ministries">Ministries</a></li>
                          <li><a href="#admin-about-leadership">Leadership</a></li>
                          <li><a href="#admin-about-mission">Missions</a></li>
                        </ul>
                     </li>
                    <li><a href="#admin-missions">Our Missions</a></li>
                    <li><a href="#logout">Log Out</a></li>
                </ul>
            </div>

            <!-- Content -->
            <div class="col-md-9">
                <div class="tab-content" id="admin-tabbed-content">
                    <div class="tab-pane fade in active" id="admin-home">
                       <h2>Image Carousel <button type="button" class="btn btn-success">    <?= glyphicon("plus-sign") ?></button></h2>
                       <ul class="list-group">
                            <? generateCarouselEditor(); ?>
                       </ul>
                    </div>
                    <div class="tab-pane fade " id="admin-news">
                       <h2>News/Events</h2>
                       <form id="admin-news-add" action="admin.php?action=addnews" method="post">
                            <hr><h1>Add news item/event</h1><hr>
                            <label>Title:</label><br>
                            <input type="text" name="title"</input><br>
                            <br>
                            <label>Information:</label><br>
                            <textarea name="info"></textarea><br>
                            <br>
                            <label>Start Date: </label><br>
                            <input type="date" name="sdate"></input><br>
                            <label>End Date: </label><br>
                            <input type="date" name="edate"></input><br>
                            <br>
                            <label>Upload Image</label><br>
                            <input type="upload"></input><br>
                            <br>
                            <input type="submit" value="Save"/>
                       </form>
                       <div id="service-table-container" class="container-fluid">
                        <div class="jumbotron table-responsive" id="admin-news-list">
                            <table id="admin-services-list" class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Information</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Images</th>
                                    </tr>
                                </thead>
                                <tbody id="service-table">
                                    <?
                                    //require(CONTROLS_PATH."news.php");
                                    ?>
                                </tbody>
                             </table>
                        </div>
        </div>
                    </div>
                    <div class="tab-pane fade " id="admin-about">
                       <h2>Edit About Information</h2>
                       <form id="admin-about-story">
                            <h1>Who we are</h1><br>
                            <hr>
                            <label>Our Mission</label><br>
                            <textarea></textarea><br>
                            <button>Save Changes</button>
                            <br><br>
                            <label>Our Vision</label><br>
                            <textarea></textarea><br>
                            <button>Save Changes</button>
                            <br><br>
                            <label>Statement of Faith</label><br>
                            <textarea></textarea><br>
                            <button>Save Changes</button>
                       </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div><!-- Admin Scope -->
