<div class="modal fade" id="video-player" tabindex="-1" role="dialog" aria-labelledby="video-player-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="video-player-label"><?=$title?>
            <button type="button" id="#modal-close" class="btn pull-right" data-dismiss="modal">
                <?= glyphicon("remove-circle") ?>
            </button>
        </h4>
      </div>
      <div class="modal-body">
          <video class="video-container" src="<?=$video?>" height="100%" frameborder="0" allowfullscreen video controls >
          </video>
      </div>
    </div>
  </div>
</div>
