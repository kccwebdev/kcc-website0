<div class="panel panel-normal" id="login-panel">
    <div id="login-body" class="panel-body text-left form-normal">
        <h1>Login to KCC website</h1>
        <div id="login-area" class="container-fluid">
            <p>Please enter your information below.</p>

            <!-- Login form  -->
            <form id="login-form" onsubmit="loginSubmit(); return false;">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input required type="email" style="width:400px;" class="form-control" id="email" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input required type="password" style="width:400px;" class="form-control" id="password" placeholder="Enter password">
                </div>
                <button id="submit-button" type="submit" class="btn btn-default">Log in</button>
            </form> 
            <br> 
            <p>When you're done, click the button above to log in.</p>
        </div>
    </div>
    <div id="login-loading" class="loading-message"><h3>Logging in...</h3></div>
</div><!-- Loading Scope -->
