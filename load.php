<?php

    //import helpers
    @require_once("includes/helpers.php");

    if(isset($_POST["page"]))
    {
        $page = ($_POST["page"] !== "") ? $_POST["page"] : "home"; 
        //get the path from post
        render($page);
    }

    if(isset($_POST['sermon']))
    {
        if(!is_numeric($id = $_POST['sermon']))
        {
            exit;
        }
        require('sermons.php');
        generateSermon($id);
    }

?>
