<?php
    
    @require_once("includes/helpers.php");

    //prepare for logins
    function getMembersTab()
    {
        if(isset($_SESSION["id"]))
        {
            $link = "admin";
            $text = "Admin.";
            $logout = "<li><a href=\"#logout\" onclick=\"logout(); return false;\">" . glyphicon("off") . "</a></li>";
        }
        else
        {
            $link = "login";
            $text = "Sign in";
        }

        echo "<li id=\"$link-tab\"><a href=\"#$link\">$text</a></li>";
        if(isset($logout))
            echo $logout;
    }

    $title = "Karen Community Church";

    //load the header 
    require(HEADER_PATH);
     //load  newstream
    //require(NEWSTREAM_PATH);

    //load load.php (haha)
    require(LOAD_PATH);

   
    
    //load the footer
    require(FOOTER_PATH);

    require(NOSCRIPT_PATH);

?>
