<?php

    //import the constants
    @require_once("constants.php");

    //start the session
    session_start();

    //renders the page based on the passed data
    function render($title, $data = array())
    {
        //buld the path to the page's controller and view
        $controller = CONTROLS_PATH . $title . ".php";
        $view = VIEWS_PATH . $title . ".php"; //because this is rendered after controller, the controller can ovewrite it.

        //extract variables
        extract($data);

        //open tab
        echo "<section id=\"$title-content\">";

        //render the controller if it exists
        if(file_exists($controller))
            require($controller);

        //render the view in view (haha)  of the controller
        if(file_exists($view))
            require($view);
        else
            echo "<h1>Error 404:</h1> \"$view\", file not found.";


        //close tab
        echo "</section> <!-- end $title -->";

    }

    function query(/* $sql [, ... ] */)
    {
        // SQL statement
        $sql = func_get_arg(0);

        // parameters, if any
        $parameters = array_slice(func_get_args(), 1);

        // try to connect to database
        static $handle;
        if (!isset($handle))
        {
                try
                {
                        // connect to database
                        $handle = new PDO("mysql:dbname=" . DATABASE . ";host=" . SERVER, USERNAME, PASSWORD);

                        // ensure that PDO::prepare returns false when passed invalid SQL
                        $handle->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                }
                catch (Exception $e)
                {
                        // trigger (big, orange) error
                        trigger_error($e->getMessage(), E_USER_ERROR);
                        exit;
                }
        }

        // prepare SQL statement
        $statement = $handle->prepare($sql);
        if ($statement === false)
        {
                $error_x = $handle->errorInfo();
                // trigger (big, orange) error
                trigger_error($error_x[2], E_USER_ERROR);
                exit;
        }

        // execute SQL statement
        $results = $statement->execute($parameters);

        // return result set's rows, if any
        if ($results !== false)
        {
                return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
        else
        {
                return false;
        }
    }

    function require_string($filename, $data = array())
    {
        ob_start();
        extract($data);
        include $filename;
        return ob_get_clean();
    }

    function glyphicon($glyphicon)
    {
        return "<span class=\"glyphicon glyphicon-$glyphicon\"></span>";
    }

    function abbreviate($string, $length)
    {
        return (strlen($string) > $length) ? ("<abbr title=\"$string\">" . substr($string, 0, $length) . "</abbr>") : $string;
    }

    function prepareStringForHTML($string)
    {
        return strtolower(str_replace(" ", "", preg_replace("/[^a-zA-Z0-9\s]/", "", $string)));
    }

    function selectChurchInfo($name)
    {
        $temp = query("SELECT * FROM `info` WHERE `name`='$name'");
        return $temp[0]["information"];
    }

    function escape($string)
    {
        return htmlspecialchars($string);
    }
    function pre($string){
        echo "<pre>";
                print_r($string);
        echo "</pre>";
    }
    
?> 
