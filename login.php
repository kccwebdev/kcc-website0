<?php
    
    require_once("includes/helpers.php"); 

    function validateUser()
    {
        if(isset($_POST["email"]) && isset($_POST["password"]))
        {
            $email = $_POST["email"];
            $pass = $_POST["password"];
                //check email and password
                $user = query("SELECT * FROM `people` AS p INNER JOIN `users` AS u ON p.id = u.id WHERE p.email = '$email' AND u.pass = '$pass'");
                if(count($user)==1){
                    return $user[0];
                }else{
                    //Return JSON to login.js and show error
                    errorJSON();
                }
            //implement unregistered user for email or invalid password
            return null;
        }

        return null;
    }
    
    function attemptLogin()
    {
        $user = validateUser();
        if($user !== null)
        {
            //set the user id
            $_SESSION["id"] = $user["id"];

            //get admin page
            accessAdmin();

            //return true just in case (though code should never execute)
            return true;
        }else{
            return false;
        }
    }

    if(!attemptLogin())
    {
        //errorJSON();
        //prepare page
        $html = require_string(VIEWS_PATH . "login-form.php");

        //prepare response
        $json = array("status" => 200, "page" => "login", "html" => $html);

        //return json to ajax query
        echo json_encode($json);

        //exit for good measure
        exit();
    }


?>
