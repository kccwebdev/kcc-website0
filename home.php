<?php
    function generateCarouselItems()
    {
        $images = query("SELECT `information`,`path` FROM `media` WHERE `type`='carousel'");

        foreach($images as $image)
        {
            extract($image);

            ?>
                <div class="boxpanel">
                    <img src="<?= $path ?>" description="<?=$information?>" alt="image"/>
                </div>
            <?

        }
    }

    
    $headline = query("SELECT `information` FROM `info` WHERE `name`='headline'");
    $headline = $headline[0]["information"];
    $introduction = query("SELECT `information` FROM `info` WHERE `name`='introduction'");
    $introduction = $introduction[0]["information"];

    $example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    

    //trims the length to a certain length
    function trimLength(&$string, $length)
    {
        //leave string as is if length <= length
        if(strlen($string) <= $length) return;

        //shorten string to length
        $string = substr(ltrim($string), 0, $length);

        //trim string if necessary, storing whether or not in whole
        if($whole = (substr($string, -1) == " ")) $string = trim($string);

        //split up string
        $split = explode(" ", $string);

        //assemble string
        $string = implode(" ", $whole ? $split : array_splice($split, 0, sizeof($split) - 1)) . "...";
    }

    //get news items
    function getNewsItems()
    {
        //get news items
        $items = query("SELECT n.id,n.name,n.information,n.article FROM `news_items`  AS n
                            
                        ORDER BY n.startdate DESC LIMIT 4");

        //iterate through items, printing out news
        //pre($items);
        foreach($items as $story)
        {
            //extract story into memory
            extract($story);

            //trim information
            trimLength($information, 130);

            $link = $link = (is_null($article)) ? "#updates" : "#articles-" . $article ;

            //print out story
            include(VIEWS_PATH . "story.php");
        }
    }

    //find newest sermon
    $sermons = query('SELECT a.id FROM `sermons` AS a INNER JOIN `services` AS b on a.id = b.sermon ORDER BY b.date DESC LIMIT 1');
    $last = isset($sermons[0]['id']) ? $sermons[0]['id'] : 1;
?>
